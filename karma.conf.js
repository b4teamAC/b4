'use strict';

const webpackCfg = require('./webpack.config')('test');

module.exports = function karmaConfig(config) {

    config.set({
        browsers: ['PhantomJS'],
        files: [
            'test/loadtests.js',
          {pattern: 'src/static/menu/left-items.json', watched: false, included: false, served: true},
          {pattern: 'src/static/menu/right-items.json', watched: false, included: false, served: true},
          {pattern: 'src/static/details/header.json', watched: false, included: false, served: true},
          {pattern: 'src/static/details/subHeader.json', watched: false, included: false, served: true},
          {pattern: 'src/static/details/detailMain.json', watched: false, included: false, served: true}
        ],
        proxies: {
          '/static/menu/': 'http://localhost:8080/base/src/static/menu/',
          '/static/details/': 'http://localhost:8080/base/src/static/details/'
        },
      port: 8080,
        captureTimeout: 60000,
        logLevel: config.LOG_INFO,
        frameworks: [
            'mocha',
            'chai',
            'sinon'
        ],
        client: {
            mocha: {},
        },
        browserConsoleLogOptions: {
            level: 'log',
            format: '%b %T: %m',
            terminal: true
        },
        singleRun: true,
        reporters: ['mocha', 'coverage', 'junit'],
        mochaReporter: {
            output: 'autowatch'
        },
        preprocessors: {
            'test/loadtests.js': ['webpack', 'sourcemap']
        },
        webpack: webpackCfg,
        webpackServer: {
            noInfo: false
        },
        junitReporter: {
            outputDir: 'coverage',
            outputFile: 'junit-result.xml',
            useBrowserName: false
        },
        coverageReporter: {
            dir: 'coverage/',
            watermarks: {
                statements: [70, 80],
                functions: [70, 80],
                branches: [70, 80],
                lines: [70, 80]
            },
            reporters: [
                { type: 'text' },
                {
                    type: 'html',
                    subdir: 'html'
                },
                {
                    type: 'cobertura',
                    subdir: 'cobertura'
                },
                {
                    type: 'lcovonly',
                    subdir: 'lcov'
                }
            ]
        }
    });
};
