import React from 'react';
import MenuReducer from 'reducers/menu-reducer';

describe('MenuReducer', function () {

    let defaultState = {
            items: [],
            rightItems: []
        },
        action,
        returnState;

    beforeEach(function () {
    });

    afterEach(function () {
    });

    describe('GET_MENU_ITEMS', function () {
        it('Should return expected state', () => {
            action = {
                type: 'GET_MENU_ITEMS',
                payload: {
                    "items": [
                        {"name": "Home", "main": false},
                        {"name": "Sign in to My4", "main": true},
                        {"name": "Watch Live", "main": false},
                        {"name": "Categories", "main": false},
                        {"name": "Box Sets", "main": false},
                        {"name": "Catch Up", "main": false}
                    ],
                    "image": [{"url": "http://static.channel4.com/globalnavv1/1.0.376/images/header-logo.svg"}]
                }
            };
            returnState = MenuReducer(defaultState, action);
            for (let i = 0; i < returnState.items.items.length; i++) {
                expect(returnState.items.items[i].name).to.equal(action.payload.items[i].name);
                expect(returnState.items.items[i].main).to.equal(action.payload.items[i].main);
            }
            expect(returnState.items.image[0].url).to.equal(action.payload.image[0].url);
        })
    });

    describe('GET_RIGHT_ITEMS', () => {
        it('Should return expected state', () => {
            action = {
                type: 'GET_RIGHT_ITEMS',
                payload: {
                    parental: [
                        {
                            lockedUrl: "../static/menu/locked-lock.png",
                            unlockedUrl: "../static/menu/open-lock.svg",
                            itemTitle: "Parental Control:"
                        }
                    ],
                    search: [
                        {
                            logoUrl: "../static/menu/search.svg.png",
                            itemTitle: "Search"
                        }
                    ]
                }
            };

            returnState = MenuReducer(defaultState, action);
            expect(returnState.rightItems.parental[0].lockedUrl).to.equal(action.payload.parental[0].lockedUrl);
            expect(returnState.rightItems.parental[0].unlockedUrl).to.equal(action.payload.parental[0].unlockedUrl);
            expect(returnState.rightItems.parental[0].itemTitle).to.equal(action.payload.parental[0].itemTitle);
            expect(returnState.rightItems.search[0].logoUrl).to.equal(action.payload.search[0].logoUrl);
            expect(returnState.rightItems.search[0].itemTitle).to.equal(action.payload.search[0].itemTitle);
        });
    })
});
