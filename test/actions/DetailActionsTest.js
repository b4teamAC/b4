import React from 'react';
import * as detailActions from 'actions/detail-actions';
import apis from 'static/constants'

describe('Detail-actions', () => {

    let getHeaderDataAction = {
            success: {
                type: 'GET_HEADER',
                payload: {
                    title: "Catastrophe",
                    buttons: [
                        {
                            name: "My List",
                            image: "../../static/details/plus.png"
                        },
                        {
                            name: "Remind me",
                            image: "../../static/details/bell.svg"
                        },
                        {
                            name: "About",
                            image: "../../static/details/info.svg"
                        }
                    ]
                }
            },
            error: {
                type: 'GET_HEADER_REJECTED',
                payload: 'NOT FOUND'
            }
        },
        getSubHeaderDataAction = {
            success: {
                type: 'GET_SUB_HEADER',
                payload: [
                    {"name": "Home"},
                    {"name": "Episodes"},
                    {"name": "Clip & Extras"}
                ]
            },
            error: {
                type: 'GET_SUB_HEADER_REJECTED',
                payload: 'NOT FOUND'
            }
        },
        getDetailMainDataAction = {
            success: {
                type: 'GET_MAIN_DETAIL',
                payload: {
                    "visitorId": "12345",
                    "videoId": "10002334",
                    "duration": "1000",
                    "genre": "comedy",
                    "manifestUrl": "http://de9b7h88wgj5l.cloudfront.net/static/sintel/set1_hls_mbr.m3u8",
                    "coverImage": "http://ic.c4assets.com/brands/baby-daddy/series-5/episode-9/148b56dd-8b70-4c67-ab53-0de716a7299e_625x352.jpg?interpolation=progressive-bicubic&output-quality=90&output-format=jpeg&resize=1024px:576px&output-format=jpeg",
                    "adurl": {
                        "type": "Primetime Ads",
                        "details": {
                            "domain": "auditude.com",
                            "mediaid": "noidaPT_asset_15Seconds_2Slots_RequestId4",
                            "zoneid": "264573"
                        }
                    },
                    "title": "Series 3 Episode 2",
                    "description": "Things are still shaky between Sharon and Rob, and when she returns to teaching, Rob seeks a new job. Taking over the school run, Rob is plunged into playground politics.",
                    "footerTop": "First shown: 7 Mar 2017",
                    "footerBottom": "Show Clips & Extras"
                }
            },
            error: {
                type: 'GET_MAIN_DETAIL_REJECTED',
                payload: 'NOT FOUND'
            }
        },
        dispatchFunction,
        stubDispatch;

    describe('.getHeaderData()', () => {
        it('Success scenario', (done) => {
            dispatchFunction = detailActions.getHeaderData(apis.getHeaderData);
            stubDispatch = (action) => {
                expect(action.type).to.equal(getHeaderDataAction.success.type);
                expect(
                    JSON.stringify(action.payload) === JSON.stringify(getHeaderDataAction.success.payload)
                ).to.be.true;
                done();
            };
            dispatchFunction(stubDispatch);
        });

        it('Error scenario', (done) => {
            dispatchFunction = detailActions.getHeaderData(apis.getHeaderData + '/invalidPath');
            stubDispatch = (action) => {
                expect(action.type).to.equal(getHeaderDataAction.error.type);
                expect(action.payload).to.equal(getHeaderDataAction.error.payload);
                done();
            };
            dispatchFunction(stubDispatch);
        });
    });

    describe('.getSubHeaderData()', () => {
        it('Success scenario', (done) => {
            dispatchFunction = detailActions.getSubHeaderData(apis.getSubHeaderData);
            stubDispatch = (action) => {
                expect(action.type).to.equal(getSubHeaderDataAction.success.type);
                expect(
                    JSON.stringify(action.payload) === JSON.stringify(getSubHeaderDataAction.success.payload)
                ).to.be.true;
                done();
            };
            dispatchFunction(stubDispatch);
        });

        it('Error scenario', (done) => {
            dispatchFunction = detailActions.getSubHeaderData(apis.getSubHeaderData + '/invalidPath');
            stubDispatch = (action) => {
                expect(action.type).to.equal(getSubHeaderDataAction.error.type);
                expect(action.payload).to.equal(getSubHeaderDataAction.error.payload);
                done();
            };
            dispatchFunction(stubDispatch);
        });
    });

    describe('.getDetailMainData()', () => {
        it('Success scenario', (done) => {
            dispatchFunction = detailActions.getDetailMainData(apis.getDetailMainData);
            stubDispatch = (action) => {
                expect(action.type).to.equal(getDetailMainDataAction.success.type);
                expect(
                    JSON.stringify(action.payload) === JSON.stringify(getDetailMainDataAction.success.payload)
                ).to.be.true;
                done();
            };
            dispatchFunction(stubDispatch);
        });

        it('Error scenario', (done) => {
            dispatchFunction = detailActions.getDetailMainData(apis.getDetailMainData + '/invalidPath');
            stubDispatch = (action) => {
                expect(action.type).to.equal(getDetailMainDataAction.error.type);
                expect(action.payload).to.equal(getDetailMainDataAction.error.payload);
                done();
            };
            dispatchFunction(stubDispatch);
        });
    });

});
