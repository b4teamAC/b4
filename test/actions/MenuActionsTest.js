import React from 'react';
import * as menuActions from 'actions/menu-actions';
import apis from 'static/constants'

describe('Menu-actions', () => {

    let leftNavigationAction = {
            success: {
                type: 'GET_MENU_ITEMS',
                payload: {
                    "items": [
                        {"name": "Home", "main": false},
                        {"name": "Sign in to My4", "main": true},
                        {"name": "Watch Live", "main": false},
                        {"name": "Categories", "main": false},
                        {"name": "Box Sets", "main": false},
                        {"name": "Catch Up", "main": false}
                    ],
                    "image": [{"url": "http://static.channel4.com/globalnavv1/1.0.376/images/header-logo.svg"}]
                }
            },
            error: {
                type: 'GET_MENU_ITEMS_REJECTED',
                payload: 'NOT FOUND'
            }
        },
        rightNavigationAction = {
            success: {
                type: 'GET_RIGHT_ITEMS',
                payload: {
                    parental: [
                        {
                            lockedUrl: "../static/menu/locked-lock.png",
                            unlockedUrl: "../static/menu/open-lock.svg",
                            itemTitle: "Parental Control:"
                        }
                    ],
                    search: [
                        {
                            logoUrl: "../static/menu/search.svg.png",
                            itemTitle: "Search"
                        }
                    ]
                }
            },
            error: {
                type: 'GET_RIGHT_ITEMS_REJECTED',
                payload: 'NOT FOUND'
            }
        },
        dispatchFunction,
        stubDispatch;

    describe('getLeftNavigation()', () => {
        it('Success scenario', (done) => {
            dispatchFunction = menuActions.getLeftNavigation(apis.getLeftNavigation);
            stubDispatch = (action) => {
                expect(action.type).to.equal(leftNavigationAction.success.type);
                expect(
                    JSON.stringify(action.payload) === JSON.stringify(leftNavigationAction.success.payload)
                ).to.be.true;
                done();
            };
            dispatchFunction(stubDispatch);
        });

        it('Error scenario', (done) => {
            dispatchFunction = menuActions.getLeftNavigation(apis.getLeftNavigation + '/invalidPath');
            stubDispatch = (action) => {
                expect(action.type).to.equal(leftNavigationAction.error.type);
                expect(action.payload).to.equal(leftNavigationAction.error.payload);
                done();
            };
            dispatchFunction(stubDispatch);
        });
    });

    describe('getRightNavigation()', () => {
        it('Success scenario', (done) => {
            dispatchFunction = menuActions.getRightNavigation(apis.getRightNavigation);
            stubDispatch = (action) => {
                expect(action.type).to.equal(rightNavigationAction.success.type);
                expect(
                    JSON.stringify(action.payload) === JSON.stringify(rightNavigationAction.success.payload)
                ).to.be.true;
                done();
            };
            dispatchFunction(stubDispatch);
        });

        it('Error scenario', (done) => {
            dispatchFunction = menuActions.getRightNavigation(apis.getRightNavigation + '/invalidPath');
            stubDispatch = (action) => {
                expect(action.type).to.equal(rightNavigationAction.error.type);
                expect(action.payload).to.equal(rightNavigationAction.error.payload);
                done();
            };
            dispatchFunction(stubDispatch);
        });
    });
});
