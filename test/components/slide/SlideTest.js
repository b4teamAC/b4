import React from 'react';
import {shallow} from 'enzyme';
import Slide from 'components/slide/Slide.js';

xdescribe('<Slide />', function () {

    let component;
    beforeEach(function () {
        component = shallow(<Slide />);
    });

    describe('when rendering the component', function () {

        it('should have a className of "slide"', function () {
            expect(component.class).to.contain('slide-cssmodule-slide');
        });
    });
});
