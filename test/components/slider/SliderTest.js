import React from 'react';
import {shallow} from 'enzyme';
import Slider from 'components/slider/Slider.js';

xdescribe('<Slider />', function () {

    let component;
    beforeEach(function () {
        component = shallow(<Slider />);
    });

    describe('when rendering the component', function () {

        it('should have a className of "slider-component"', function () {
            expect(component.hasClass('slider-component')).to.equal(true);
        });
    });
});
