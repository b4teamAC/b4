import React from 'react';
import {shallow} from 'enzyme';
import {Menu} from 'components/menu/menu';
import ListItems from 'components/menu/list-items'
import ParentalControl from 'components/menu/parental-control'
import Search from 'components/menu/search'

describe('Menu component', function () {

    beforeEach(function () {
        this.component = shallow(<Menu />);
    });

    describe('', function () {
        it('Should contain ListItem component', function () {
            expect(this.component.find(ListItems)).to.have.length(1);
        });

        it('Should contain ParentalControl component', function () {
            expect(this.component.find(ParentalControl)).to.have.length(1);
        });

        it('Should contain Search component', function () {
            expect(this.component.find(Search)).to.have.length(1);
        });
    });

});
