import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import {ListItems} from 'components/menu/list-items';

describe('Sub-Header component', function () {

    let props = {
            dispatch: function () {
            },
            image: [{"url": "http://static.channel4.com/globalnavv1/1.0.376/images/header-logo.svg"}],
            items: [
                {"name": "Home", "main": false},
                {"name": "Sign in to My4", "main": true},
                {"name": "Watch Live", "main": false},
                {"name": "Categories", "main": false},
                {"name": "Box Sets", "main": false},
                {"name": "Catch Up", "main": false}
            ]
        },
        spy_dispatch,
        spy_componentWillMount,
        spy_getLogo,
        spy_getList;

    beforeEach(function () {
        spy_componentWillMount = sinon.spy(ListItems.prototype, 'componentWillMount');
        spy_dispatch = sinon.spy(props, 'dispatch');
        spy_getLogo = sinon.spy(ListItems.prototype, 'getLogo');
        spy_getList = sinon.spy(ListItems.prototype, 'getList');

        let element = (<ListItems dispatch={props.dispatch}/>);
        const wrapper = shallow(element);
        this.component = wrapper.instance();
    });

    afterEach(function () {
        spy_dispatch.restore();
        spy_componentWillMount.restore();
        spy_getLogo.restore();
        spy_getList.restore();
    });

    describe('componentWillMount()', function () {
        it('Should call componentWillMount()', () => {
            expect(spy_componentWillMount.calledOnce).to.be.true;
        });

        it('Should call dispatch()', () => {
            expect(spy_dispatch.calledOnce).to.be.true;
        });
    });

    describe('.getLogo()', function () {
        it('Should call the function and render expected set of elements.', function () {
            let result = ListItems.prototype.getLogo(props.image);
            expect(spy_getLogo.called).to.be.true;
            for (let i = 0; i < result.length; i++) {
                expect(result[i].key).to.equal(props.image[0].url);
                expect(result[i].props.styleName).to.equal('list-item logo main');
            }
        });
    });

    describe('.getList()', () => {
        it('Should call the function and render expected set of elements', () => {
            let result = ListItems.prototype.getList(props.items);
            expect(spy_getList.called).to.be.true;
            for (let i = 0; i < result.length; i++) {
                expect(result[i].key).to.equal(props.items[i].name);
            }
        });
    });
});
