import React from 'react';
import {shallow, mount} from 'enzyme';
import sinon from 'sinon';
import {SubHeader} from 'components/detail/sub-header';

describe('Sub-Header component', function () {

    let props = {
            dispatch: function () {
            }
        },
        spy_componentWillMount,
        spy_dispatch,
        spy_getButtons;

    beforeEach(function () {
        spy_componentWillMount = sinon.spy(SubHeader.prototype, 'componentWillMount');
        spy_dispatch = sinon.spy(props, 'dispatch');
        spy_getButtons = sinon.spy(SubHeader.prototype, 'getButtons');
        let element = (<SubHeader dispatch={props.dispatch}/>);
        const wrapper = shallow(element);
        this.component = wrapper.instance();
    });

    afterEach(function () {
        spy_componentWillMount.restore();
        spy_dispatch.restore();
        spy_getButtons.restore();
    });

    describe('componentWillMount()', function () {
        it('Should call componentWillMount()', () => {
            expect(spy_componentWillMount.calledOnce).to.be.true;
        });

        it('Should call dispatch()', () => {
            expect(spy_dispatch.calledOnce).to.be.true;
        });
    });

    describe('.getButtons()', function () {
        it('Should be defined', function () {
            expect(spy_getButtons).to.be.defined;
        });

        it('Should render elements based on given parameter data.', function () {
            let data = [
                {"name": "Home"},
                {"name": "Episodes"},
                {"name": "Clip & Extras"}
            ];
            let result = SubHeader.prototype.getButtons.apply(this.component, [data]);
            expect(spy_getButtons.called).to.be.true;

            for (let i = 0; i < result.length; i++) {
                expect(parseInt(result[i].key)).to.equal(i);
                expect(result[i].type).to.equal('div');
                expect(result[i].props.children).to.equal(data[i].name);
                expect(result[i].props.styleName).contains('sub-header-item');
            }
        });
    });

    describe('Initial state set.', function () {
        it('activeTab should be zero', function () {
            expect(this.component.state.activeTab).to.equal(0);
        });
    });
});
