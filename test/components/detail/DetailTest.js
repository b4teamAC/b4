import React from 'react';
import {shallow} from 'enzyme';

import {Detail} from 'components/detail/detail';

import Menu from 'components/menu/menu';
import DetailHeader from 'components/detail/detail-header'
import SubHeader from 'components/detail/sub-header'
import DetailMain from 'components/detail/detail-main'

describe('Main detail component', function () {

    beforeEach(function () {
        this.component = shallow(<Detail />);
    });

    describe('Detail component render', function () {
        it('Should contain Menu component', function () {
            expect(this.component.find(Menu)).to.have.length(1);
        });

        it('Should contain DetailHeader component', function () {
            expect(this.component.find(DetailHeader)).to.have.length(1);
        });

        it('Should contain SubHeader component', function () {
            expect(this.component.find(SubHeader)).to.have.length(1);
        });

        it('Should contain DetailMain component', function () {
            expect(this.component.find(DetailMain)).to.have.length(1);
        });

    });
});
