import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import {DetailHeader} from 'components/detail/detail-header';

describe('Detail Header component', function () {

    let props = {
            title: 'myTitle',
            dispatch: function () {
            },
            buttonData: [
                {"name": "My List", "image": "../../static/details/plus.png"},
                {"name": "Remind me", "image": "../../static/details/bell.svg"},
                {"name": "About", "image": "../../static/details/info.svg"}
            ]
        },
        element,
        spy_dispatch,
        spy_render,
        spy_componentWillMount,
        spy_getHeader;

    beforeEach(function () {
        spy_render = sinon.spy(DetailHeader.prototype, 'render');
        spy_componentWillMount = sinon.spy(DetailHeader.prototype, 'componentWillMount');
        spy_getHeader = sinon.spy(DetailHeader.prototype, 'getHeader');
        spy_dispatch = sinon.spy(props, 'dispatch');

        element = (<DetailHeader title={props.title} dispatch={props.dispatch}/>);
        const wrapper = shallow(element);
        this.component = wrapper.instance();
    });

    afterEach(function () {
        spy_dispatch.restore();
        spy_render.restore();
        spy_componentWillMount.restore();
        spy_getHeader.restore();
    });

    describe('Main render()', function () {
        it('Should call render()', () => {
            expect(spy_render.called).to.be.true;
        });
    });

    describe('.getHeader()', function () {
        it('Should call .getHeader() and render expected set of elements.', function () {
            let result = DetailHeader.prototype.getHeader(props.title, props.buttonData);
            expect(spy_getHeader.called).to.be.true;
            expect(result.props.children[0].props.styleName).to.equal('header-title');
            for (var i = 0; i < result.props.children[0].props.children.length; i++) {
                expect(result.props.children[0].props.children[i].styleName).to.equal('header-btn')
            }
            expect(result.props.children[0].props.children.props.styleName).to.equal('title');
            expect(result.props.children[0].props.children.props.children).to.equal(props.title);
            expect(result.props.children[1].props.styleName).to.equal('header-buttons');
        });
    });

    describe('componentWillMount()', function () {
        it('Should call componentWillMount()', () => {
            expect(spy_componentWillMount.called).to.be.true;
        });

        it('Should call dispatch inside componentWillMount', function () {
            expect(spy_dispatch.calledOnce).to.be.true;
        });
    });

});
