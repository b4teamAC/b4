/* CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
import React, {
    Component,
    PropTypes
} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {} from '../actions/';
import Main from '../components/App';
import {Router, Route, browserHistory} from 'react-router';
import Detail from '../components/detail/detail';

/* Populated by react-webpack-redux:reducer */
class App extends Component {
    render() {
        const {actions} = this.props;
        return (
          <Router history={browserHistory}>
            <Route path={'/'} component={Main}/>
            <Route path={'/detail:id'} component={Detail}/>
            <Main actions={actions}/>
            <Route path="*" component={Main}/>
          </Router>
        );
    }
}

/* Populated by react-webpack-redux:reducer
 *
 * HINT: if you adjust the initial type of your reducer, you will also have to
 *       adjust it here.
 */
App.propTypes = {
    actions: PropTypes.shape({})
};
function mapStateToProps(state) { // eslint-disable-line no-unused-vars
    /* Populated by react-webpack-redux:reducer */
    const props = {};
    return props;
}
function mapDispatchToProps() {
    /* Populated by react-webpack-redux:action */
    const actions = {};
    const actionMap = {
        // actions: bindActionCreators(actions, dispatch)
    };
    return actionMap;
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
