const apis = {
    getLeftNavigation: 'static/menu/left-items.json',
    getRightNavigation: 'static/menu/right-items.json',
    getHeaderData: 'static/details/header.json',
    getSubHeaderData: 'static/details/subHeader.json',
    getDetailMainData: 'static/details/detailMain.json'
};

export default apis;
