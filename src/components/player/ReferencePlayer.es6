import ReferencePlayerEvents from "./ReferencePlayerEvents";
import ReferencePlayerEventStatus from "./ReferencePlayerEventStatus";
import ReferencePlayerUtils from "./ReferencePlayerUtils";
import ExtCueOutContentFactory from "./generators/ExtCueOutContentFactory";
import CustomAdPolicySelector from "./addOns/CustomAdPolicySelector";
import PlayerControls from "./addOns/PlayerControls";
import CompanionAds from "./addOns/CompanionAds";
import ClickableAds from "./addOns/ClickableAds";
import Captions from "./addOns/Captions";
import Ads from "./addOns/Ads";
import LBA from "./addOns/LBA";
import ConsoleLogs from "./addOns/ConsoleLogs";
import BufferingOverlay from "./addOns/BufferingOverlay";
import QoS from "./addOns/QoS";
import DRM from "./addOns/DRM";
import ID3 from "./addOns/ID3";
import LogPlayerEvents from "./addOns/LogPlayerEvents";
import TimelineMarker from "./addOns/TimelineMarker";
import VideoAnalytics from "./addOns/VideoAnalytics";

let css = require("../../static/player/css/style.css");

/**
 * Construct a new TVSDK Reference Player Client.
 *
 * @returns TVSDK Reference Player
 * @constructor
 */
class ReferencePlayer {

    constructor() {
        this.player = new AdobePSDK.MediaPlayer();
        // Setting Force flashback
        this.forceFlash = false;
        // Setting Auto Play
        this.autoPlay = true;
        this.eventListeners = {};
        this.disableControlsInAd = false;
        this.videoDiv = document.getElementById("videoDiv");
        this.videoControlsDiv = document.getElementById("video-controls");
        this.fullScreenDiv = document.getElementById("mainVideoDiv");
        this.prevMediaResource = null;
        this.prevConfig = null;
        this.playerControlsAddOn = null;
        this.companionAdsAddOn = null;
        this.drmAddOn = null;
        this.bufferingAddOn = null;
        this.clickableAdsAddOn = null;
        this.consoleAddOn = null;
        this.qosAddOn = null;
        this.captionsAddOn = null;
        this.lbaAddOn = null;
        this.id3AddOn = null;
        this.adsAddOn = null;
        this.logPlayerEventsAddOn = null;
        this.videoAnalyticsAddOn = null;
        this.timelineMarkerAddOn = null;
        this._customAdView = null;

        // set the view
        this.player.view = new AdobePSDK.MediaPlayerView(this.videoDiv, this.fullScreenDiv);
        // video.scalePolicy = new MediaPlayerViewCustomScalePolicy();

        // add listeners
        this.player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, this.onStatusChange.bind(this));
    }

    static getMimeTypeStringFromMediaResourceType(resourceType) {
        switch (resourceType) {
            case AdobePSDK.MediaResourceType.HLS:
                return "application/x-mpegURL";
            case AdobePSDK.MediaResourceType.DASH:
                return "application/dash+xml";
            case AdobePSDK.MediaResourceType.ISOBMFF:
                return "video/mp4";
            default:
                return null;
        }
    }

    onTimedMetadataEvent(event) {
        let timedMetadata = event.timedMetadata;
        if (timedMetadata.type == AdobePSDK.TimedMetadataType.TAG) {
            this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                message: "TimedMetadata: time: " + timedMetadata.time + " content: " + timedMetadata.content
            });
        }
    }

    onSizeAvailableEvent() {
        this.player.view.setSize(this.videoDiv.offsetWidth, this.videoDiv.offsetHeight);
    }

    onResizeEvent(event) {
        // Special processing for IOS Safari platform,
        // as Apple takes over custom player controls and displays its native controls.
        // This causes inconsistenciy in UI of player's custom controls.
        // This example demonstrates fixing UI button for full screen.
        if (ReferencePlayerUtils.isBrowserIOSSafari()) {
            this.videoDiv.querySelector("#btn_fullscreen").classList.remove("fullscreen-state");
            this.videoDiv.classList.add("videoDivMaxSize");
        }
    }

    /* For debugging */
    onStatusChange(event) {
        switch (event.status) {

            case AdobePSDK.MediaPlayerStatus.IDLE:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "Player Status: IDLE"
                });
                break;

            case AdobePSDK.MediaPlayerStatus.INITIALIZING:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: INITIALIZING"
                });
                /*add event listener to TimedMetadataEvent in initializing only.
                 * because TimedMetadataEvent comes in initializing player state.
                 */
                this.getPlayer().addEventListener(AdobePSDK.PSDKEventType.TIMED_METADATA_AVAILABLE, this.onTimedMetadataEvent);
                break;

            case AdobePSDK.MediaPlayerStatus.INITIALIZED:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: INITIALIZED"
                });
                this.player.prepareToPlay(AdobePSDK.MediaPlayer.LIVE_POINT);
                break;

            case AdobePSDK.MediaPlayerStatus.PREPARING:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: PREPARING"
                });
                break;

            case AdobePSDK.MediaPlayerStatus.PREPARED:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: PREPARED"
                });

                if (this._customAdView && this._customAdView.parentNode) {
                    this._customAdView.parentNode.removeChild(this._customAdView);
                }

                let customAdView = this.player.getCustomAdView();
                this.player.setCustomAdTimeout(11 * 1000); // Setting custom ad timeout to be 11 seconds
                if (customAdView) {
                    customAdView.style.zIndex = "1000";
                    this.videoDiv.insertBefore(customAdView, this.videoDiv.firstChild);
                }

                this._customAdView = customAdView;
                this.qosAddOn.stop(); //Restart the QOS
                this.qosAddOn.start();

                if (this.autoPlay) {
                    this.player.play();
                }

                if (ReferencePlayerUtils.isBrowserIOSSafari() &&
                    this.videoDiv && this.videoDiv.getElementsByTagName('video')[0]) {
                    this.videoDiv.getElementsByTagName('video')[0].addEventListener("resize", this.onResizeEvent);
                }
                break;

            case AdobePSDK.MediaPlayerStatus.PLAYING:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: PLAYING"
                });
                // TODO update UI play/pause to show pause icon
                break;

            case AdobePSDK.MediaPlayerStatus.PAUSED:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: PAUSED"
                });
                // TODO update UI play/pause to show play icon & display pause icon over video
                break;

            case AdobePSDK.MediaPlayerStatus.SEEKING:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: SEEKING"
                });
                break;

            case AdobePSDK.MediaPlayerStatus.COMPLETE:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: COMPLETE"
                });
                this.dispatchEvent(ReferencePlayerEvents.StatusChangeEvent, {
                    status: ReferencePlayerEventStatus.PLAYER_STATUS_COMPLETE
                });
                this.qosAddOn.stop(); //Stop the QOS
                break;

            case AdobePSDK.MediaPlayerStatus.RELEASED:
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer onStatusChange Player Status: RELEASED"
                });
                this.qosAddOn.stop(); //Stop the QOS
                break;

            case AdobePSDK.MediaPlayerStatus.ERROR:
                let errorCode = event.metadata.getValue('PSDK_ERROR_CODE');
                let errorDesc = event.metadata.getValue('DESCRIPTION');
                let appGeneratedError;
                if (errorCode === undefined) {
                    errorCode = "Error Code : Undefined";
                } else {
                    appGeneratedError = parseInt(errorCode) === AdobePSDK.PSDKErrorCode.APP_GENERATED_ERROR;
                    errorCode = "Error Code : " + errorCode;
                }
                if (errorDesc === undefined) {
                    errorDesc = "[Demo for Custom Error] " + "Unknown Error Occurred";
                } else {
                    if (appGeneratedError === true) {
                        let appErrorCode = event.metadata.getValue("APP_ERROR_CODE");
                        let appError = event.metadata.getValue("APP_ERROR");
                        let appErrorDescription = event.metadata.getValue("APP_ERROR_DESCRIPTION");
                        errorDesc += " APP ERROR INFO: ";
                        if (appErrorCode !== null && appErrorCode !== undefined)
                            errorDesc += " APP_ERROR_CODE: " + appErrorCode;
                        if (appError !== null && appError !== undefined)
                            errorDesc += " APP_ERROR: " + appError;
                        if (appErrorDescription !== null && appErrorDescription !== undefined)
                            errorDesc += " APP_ERROR_DESCRIPTION: " + appErrorDescription;
                    }
                    errorDesc = " [Demo for Custom Error] " + errorDesc;
                }

                this.qosAddOn.stop(); //Stop the QOS

                this.reset();
                this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: errorDesc
                });
                break;
        }
    }

    replaceCurrentResource(mediaResource, config) {
        this.prevMediaResource = mediaResource;
        this.prevConfig = config;
        this.player.replaceCurrentResource(mediaResource, config);
    }

    static getResourceTypeFromURL(resourceUrl) {
        let resourceType = null,
            endsWith = function (str, suffix) {
                return str.toUpperCase().indexOf(suffix.toUpperCase(), str.length - suffix.length) !== -1;
            };

        let trimmedUrl = resourceUrl.split("?")[0];
        trimmedUrl = trimmedUrl.trim();
        if (endsWith(trimmedUrl, ".mpd") || endsWith(trimmedUrl, "(format=mpd-time-csf)")) {
            resourceType = AdobePSDK.MediaResourceType.DASH;
        } else if (endsWith(trimmedUrl, ".m3u8")) {
            resourceType = AdobePSDK.MediaResourceType.HLS;
        } else if (endsWith(trimmedUrl, ".mp4") || endsWith(trimmedUrl, ".mp3")) {
            resourceType = AdobePSDK.MediaResourceType.ISOBMFF;
        }
        return resourceType;
    }

    determineResource(resourceUrl) {
        let resourceType,
            startsWith = function (str, prefix) {
                return str.toUpperCase().indexOf(prefix.toUpperCase()) === 0;
            };

        resourceUrl = resourceUrl.trim();
        // Check if any valid resource type is appended before media url
        if (startsWith(resourceUrl, 'dash:')) {
            resourceUrl = resourceUrl.substring(resourceUrl.indexOf(':') + 1);
            resourceType = AdobePSDK.MediaResourceType.DASH;
        } else if (startsWith(resourceUrl, 'hls:')) {
            resourceUrl = resourceUrl.substring(resourceUrl.indexOf(':') + 1);
            resourceType = AdobePSDK.MediaResourceType.HLS;
        } else if (startsWith(resourceUrl, 'mp4:') || startsWith(resourceUrl, 'mp3:')) {
            resourceUrl = resourceUrl.substring(resourceUrl.indexOf(':') + 1);
            resourceType = AdobePSDK.MediaResourceType.ISOBMFF;
        } else {
            // Try to find resource type from extension
            resourceType = ReferencePlayer.getResourceTypeFromURL(resourceUrl);
        }

        if (resourceType === null) {
            this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                message: "[determineResource] Resource type not supported:" + resourceUrl
            });
            return AdobePSDK.PSDKErrorCode.INVALID_ARGUMENT;
        }

        return {
            resourceUrl: resourceUrl,
            resourceType: resourceType
        };
    }

    loadResource(contentInfo) {
        // set default ccVisibility, default CC is visible
        this.player.ccVisibility = AdobePSDK.MediaPlayer.VISIBLE;

        //clear ID3
        this.id3AddOn.cleanup();

        //clear Ad Markers
        this.timelineMarkerAddOn.cleanup();

        // Setting ABR settings
        this.player.abrControlParameters = new AdobePSDK.ABRControlParameters(
            1500000, 300000, 300000,
            AdobePSDK.ABRControlParameters.MODERATE_POLICY
        );

        let config = new AdobePSDK.MediaPlayerItemConfig(),
            mediaResource = null,
            auditudeSettings = null,
            adUrl = contentInfo.adurl,
            resource = this.determineResource(contentInfo.manifestUrl), // determine type from url
            resourceType = resource.resourceType;
        // Update after determination for non http protocols
        contentInfo.manifestUrl = resource.resourceUrl;

        if ((adUrl && adUrl.type && (adUrl.type.indexOf("Primetime Ads") > -1 ))) {
            if ((adUrl.type && adUrl.type.indexOf("Primetime Ads") > -1)) {
                auditudeSettings = new AdobePSDK.AuditudeSettings();

                //settings coming from sources list
                if (adUrl.type && adUrl.type.indexOf("Primetime Ads") > -1 && adUrl.details) {
                    auditudeSettings.domain = adUrl.details.domain;
                    auditudeSettings.mediaId = adUrl.details.mediaid;
                    auditudeSettings.zoneId = adUrl.details.zoneid;
                }

                //some default settings. those provided externally will override sources list
                // Default Settings
                auditudeSettings.validMimeTypes = ["application/x-mpegURL", "vnd.apple.mpegURL"];
                auditudeSettings.responseType = "Auditude 1.4";
                auditudeSettings.creativeRepackagingEnabled = true;

                auditudeSettings.domain = "auditude.com";
                auditudeSettings.mediaId = "noidaPT_asset_15Seconds_2Slots_RequestId4";
                auditudeSettings.zoneId = "264573";
                //auditudeSettings.responseType = "VMAP";
                auditudeSettings.creativeRepackagingEnabled = true;

                auditudeSettings.validMimeTypes = [];
                auditudeSettings.validMimeTypes.push("application/x-mpegURL");
                auditudeSettings.validMimeTypes.push("vnd.apple.mpegURL");
                //auditudeSettings.validMimeTypes.push("video/mp4");
                //auditudeSettings.validMimeTypes.push("application/dash+xml");

                // Targeting Info
                //auditudeSettings.targetingInfo = new AdobePSDK.Metadata();
                //auditudeSettings.targetingInfo.setValue(key, value);

                // Custom Parameters
                //auditudeSettings.customParameters = new AdobePSDK.Metadata();
                //auditudeSettings.customParameters.setValue(key, value);

                if (resourceType === AdobePSDK.MediaResourceType.ISOBMFF && auditudeSettings.validMimeTypes && auditudeSettings.validMimeTypes.indexOf("video/mp4") === -1) {
                    auditudeSettings.validMimeTypes.push("video/mp4");
                } else if (resourceType === AdobePSDK.MediaResourceType.DASH && auditudeSettings.validMimeTypes && auditudeSettings.validMimeTypes.indexOf("application/dash+xml") === -1) {
                    auditudeSettings.validMimeTypes.push("application/dash+xml");
                }
                // Set CRS required format.
                auditudeSettings.creativeRepackagingFormat = ReferencePlayer.getMimeTypeStringFromMediaResourceType(resourceType);
            } else if ((adUrl.type && adUrl.type.indexOf("Custom Ads") > -1)) {
                let fwObj = new FwMetadata();
                let fwAdsDetails = [{
                    "customId": "pre-roll",
                    "adUnit": tv.freewheel.SDK.ADUNIT_PREROLL,
                    "timePosition": 0
                },
                    {
                        "customId": "post-roll",
                        "adUnit": tv.freewheel.SDK.ADUNIT_POSTROLL,
                        "timePosition": 596
                    }
                ];
                fwObj.setFwAdDetails(fwAdsDetails);

                //settings coming from sources list
                if ((adUrl.type && adUrl.type.indexOf("Custom Ads") > -1 && adUrl.details)) {
                    fwObj.setNetworkId(adUrl.details.networkid);
                    fwObj.setServerUrl(adUrl.details.serverurl);
                    fwObj.setProfileId(adUrl.details.profileid);
                    fwObj.setVideoAssetId(adUrl.details.videoassetid);
                    fwObj.setSiteSectionId(adUrl.details.sitesectionid);
                    fwObj.setVideoDuration(adUrl.details.videoduration);
                    fwObj.getFwCRSZoneID(adUrl.details.crszoneid);
                }

                //those provided externally will override sources list
                fwObj = getFWAdsSettings(fwObj);

                // Set CRS required format.
                fwObj.setCRSFormat(ReferencePlayer.getMimeTypeStringFromMediaResourceType(resourceType));
            }

            let adSettings = null;
            if (auditudeSettings) {
                adSettings = auditudeSettings;
            }
            /* Unsure from where these settings might be coming
             else if (DashAdSettings)
             adSettings = DashAdSettings;
             */
            mediaResource = new AdobePSDK.MediaResource(contentInfo.manifestUrl, resourceType, adSettings, this.forceFlash);

            if (adSettings) {
                // if custom ad policy is checked
                let adPolicySelector = new CustomAdPolicySelector(
                    AdobePSDK.AdBreakWatchedPolicy.WATCHED_ON_BEGIN,
                    AdobePSDK.AdBreakPolicy.PLAY,
                    AdobePSDK.AdPolicy.PLAY);

                config.advertisingMetadata = adSettings;
                config.advertisingFactory = new ExtCueOutContentFactory(auditudeSettings, null, adPolicySelector);
                config.subscribeTags = ["#EXT-X-CUE-OUT", "#EXT-X-CUE"];
            }
        } else {
            let metadata = new AdobePSDK.Metadata();
            mediaResource = new AdobePSDK.MediaResource(contentInfo.manifestUrl, resourceType, metadata, this.forceFlash);

            config.subscribeTags = ["#EXT-X-START"];
        }

        let networkConfiguration = new AdobePSDK.NetworkConfiguration();
        networkConfiguration.useRedirectedUrl = true;
        networkConfiguration.forceNativeNetworking = false; // Stream Integrity

        config.networkConfiguration = networkConfiguration;
        // config.adSignalingMode = AdobePSDK.AdSignalingMode.MANIFEST_CUES;

        // console.log(AdobePSDK.isFlashFallbackSupported(mediaResource.type));
        this.replaceCurrentResource(mediaResource, config);

        //Adding Label for PlayerTechnology Type
        //Adding for HTML5 Player's current version info
        let currentVersion = this.player.getVersion();
        this.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "*******Player Type:  " + AdobePSDK.PlayerTechnology.getSupportedTechnology(mediaResource) + "\n" +
            "*******Player Version: " + currentVersion + "\n" +
            "*******Video id: " + contentInfo.videoId + " duration: " + contentInfo.duration
        });
    }

    getListeners(type) {
        if (!(type in this.eventListeners)) {
            this.eventListeners[type] = [];
        }

        return this.eventListeners[type];
    }

    /* Public functions */

    /**
     * Set custom video controls to this player. A video container must be set, via setVideoContainer,
     * before calling this method.
     * @param container - DOM element which contains custom video controls.
     */
    setVideoControls(container) {
        if (this.player.view) {
            this.player.view.attachVideoControls(container);
        }
    }

    /**
     * Removes all video controls from the player both custom and default controls.
     */
    removeVideoControls() {
        if (typeof this.player.view !== 'undefined') {
            this.player.view.removeVideoControls();
        }
    }

    /**
     * Load new media resource to this player. Resets player and releases any current media resource before
     * loading new resource.
     * @param {Object} contentInfo - video content meta data
     */
    loadMediaResource(contentInfo) {
        // Reset companion ads
        this.companionAdsAddOn.reset();

        let listener = function (event) {
            if (event.status === AdobePSDK.MediaPlayerStatus.IDLE) {
                this.player.removeEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, listener);
                this.loadResource(contentInfo);
            }
        };

        if (this.player.status === AdobePSDK.MediaPlayerStatus.IDLE) {
            this.reset();
            this.loadResource(contentInfo);
        } else {
            this.player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, listener);
            // first, reset player clear current resource and set state to IDLE
            this.reset();
        }
        this.player.addEventListener(AdobePSDK.PSDKEventType.SIZE_AVAILABLE, this.onSizeAvailableEvent);
    }

    loadPrevMediaResource() {
        if (this.prevConfig !== null && this.prevConfig !== undefined &&
            this.prevMediaResource !== null && this.prevMediaResource !== undefined) {
            this.player.reset();
            this.replaceCurrentResource(this.prevMediaResource, this.prevConfig);
        }
    }

    dispatchEvent(event, obj) {
        if (typeof this.eventListeners[event] !== 'undefined') {
            this.eventListeners[event].forEach(function (entry) {
                if (typeof obj !== 'undefined' && obj !== null) {
                    entry.call(this, obj);
                } else {
                    entry.call(this);
                }
            });
        }
    }

    /**
     * Fire metrics console log
     */
    logMetrics() {
        this.qosAddOn.getCurrentMetrics();
    }

    /**
     * These functions enable the additional functionality of Reference player
     * @param contentInfo - current video content meta data
     */
    loadAddOns(contentInfo) {
        // controls on video
        this.setVideoControls(this.videoControlsDiv);

        this.playerControlsAddOn = new PlayerControls(this.videoControlsDiv, this.videoDiv, this);

        this.companionAdsAddOn = new CompanionAds();
        this.companionAdsAddOn.enable(this);

        this.clickableAdsAddOn = new ClickableAds(this);
        this.clickableAdsAddOn.enableAdEvents();

        // console logs
        this.consoleAddOn = new ConsoleLogs();
        this.consoleAddOn.enableConsoleLogs(this);

        // performance logs and QoS metrics
        this.qosAddOn = new QoS();
        this.qosAddOn.enableQoSMetrics(this);

        // Enable the ad events: This will allow special handling of the player while ads are playing
        this.adsAddOn = new Ads(this);
        this.adsAddOn.enableAdEvents();

        // Enable the captions specific
        // Passing isPIPPlayer to handle toggling of CC
        this.captionsAddOn = new Captions(this);
        this.captionsAddOn.enableCaptions(this.videoDiv.classList.contains("pip-div-style"));

        // Late Bound Audio
        this.lbaAddOn = new LBA(this);
        this.lbaAddOn.enableLateBoundAudio();

        // Video Analytics
        this.videoAnalyticsAddOn = new VideoAnalytics();
        this.videoAnalyticsAddOn.enableVideoAnalytics(this.player, contentInfo);

        // event logs
        this.logPlayerEventsAddOn = new LogPlayerEvents(this);
        this.logPlayerEventsAddOn.enableEvents();

        this.id3AddOn = new ID3(this);
        this.id3AddOn.init();

        this.timelineMarkerAddOn = new TimelineMarker(this);
        this.timelineMarkerAddOn.init();

        this.bufferingAddOn = new BufferingOverlay(this.player);
        this.bufferingAddOn.enableBufferingOverlay(this.videoDiv);

        this.drmAddOn = new DRM(this);
        this.drmAddOn.enableDRMEvents();
        //drmAddOn.setDRMSettings(JSON.parse(""));
    }

    reset() {
        this.player.removeEventListener(AdobePSDK.PSDKEventType.TIMED_METADATA_AVAILABLE, this.onTimedMetadataEvent);
        this.player.removeEventListener(AdobePSDK.PSDKEventType.SIZE_AVAILABLE, this.onSizeAvailableEvent);

        if (ReferencePlayerUtils.isBrowserIOSSafari()) {
            if (this.videoDiv && this.videoDiv.getElementsByTagName('video')[0]) {
                this.videoDiv.getElementsByTagName('video')[0].removeEventListener("resize", this.onResizeEvent);
            }
        }
        this.player.reset();

        // Reset the companion ads as well
        this.companionAdsAddOn.reset();
    }

    setControlsDisabledInAd(val) {
        this.disableControlsInAd = val;
    }

    areControlsDisabledInAd() {
        return this.disableControlsInAd;
    }

    seekToLocal(position) {
        this.player.seekToLocal(position);
    }

    seekToLive() {
        this.player.seek(AdobePSDK.MediaPlayer.LIVE_POINT);
    }

    seek(position) {
        this.player.seek(position);
    }

    getSeekableRange() {
        let range = this.player.seekableRange;
        if (range === AdobePSDK.PSDKErrorCode.ILLEGAL_STATE ||
            range === AdobePSDK.PSDKErrorCode.ELEMENT_NOT_FOUND) {
            return false;
        }

        return range;
    }

    getPlaybackRange() {
        let range = this.player.playbackRange;
        if (range === AdobePSDK.PSDKErrorCode.ILLEGAL_STATE ||
            range === AdobePSDK.PSDKErrorCode.ELEMENT_NOT_FOUND) {
            return false;
        }

        return range;
    }

    /**
     * Returns the playback time currently buffered.
     * @returns the buffered range as {start, end, duration} or
     * AdobePSDK.PSDKErrorCode.ILLEGAL_STATE if the player was released or in error.
     */
    getBufferedRange() {
        return this.player.bufferedRange;
    }

    addEventListener(type, listener) {
        let listeners = this.getListeners(type),
            index = listeners.indexOf(listener);
        if (index === -1) {
            listeners.push(listener);
        }
    }

    removeEventListener(type, listener) {
        let listeners = this.getListeners(type),
            index = listeners.indexOf(listener);
        if (index !== -1) {
            listeners.splice(index, 1);
        }
    }

    getPlayer() {
        return this.player;
    }

    getVideoDiv() {
        return this.videoDiv;
    }
}

export default ReferencePlayer;
