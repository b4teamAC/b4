import React, {Component} from "react";
import ReferencePlayer from "./ReferencePlayer";
import ReferencePlayerEvents from "./ReferencePlayerEvents";
// import cssmodules from "react-css-modules";
// import styles from "./css/style.css";

class Player extends Component {

    constructor() {
        super();

        // See JSDocs for AdobePSDK for API description
        // Set relative or http path to directory containing SWF.
        // Defaults to current directory for the html page.
        AdobePSDK.setSWFPath('static/player/frameworks/player/');

        // Set the relative or http path to directory containing token file(s).
        // Defaults to SWFPath + "token/".
        AdobePSDK.setAuthorizationTokenPath('static/player/frameworks/player/token/');

        // Set the name of the token file, do not include any path in this string.
        AdobePSDK.setAuthorizationTokenFilename('token.swf');

        // Set the token type, "DAT" or "SWF". Defaults to "DAT"
        AdobePSDK.setAuthorizationTokenType('SWF');

        // Set the path to the pre-baked drm metadata used for initializing the drm system.
        AdobePSDK.setDRMMetadataPath('static/player/frameworks/player/drmMetadata/');
    }

    componentDidMount() {
        // initialize TVSDK Reference Player.
        let playerWrapper = new ReferencePlayer();

        // Optional Add-ons
        playerWrapper.loadAddOns(this.props.contentInfo);

        playerWrapper.dispatchEvent(ReferencePlayerEvents.LoadInitiatedEvent, {target: this});
        playerWrapper.loadMediaResource(this.props.contentInfo);
    }

    render() {
        return (
          <div>
            <div id="video-controls" className="video-controls-style">
              <div id="ad-row" hidden="hidden">
                <span className="cell display vid-skin-fgcolor" id="ad-remaining-time" />
                <div id="ad-seekbar" hidden="hidden">
                  <div>
                    <span className="adBacker" id="ad-backer" />
                    <span className="adProgress" id="ad-progress" />
                  </div>
                </div>
              </div>
              <div id="popup-panel">
                <div id="settings-container">
                  <div id="settings-panel-main" className="settings-panel">
                    <div id="captions-select-panel" className="settings-panel" />
                    <div id="audio-select-panel" className="settings-panel" />
                  </div>
                </div>
              </div>
              <div id="top-row">
                <div className="seekbar" id="seekbar">
                  <div>
                    <span className="backer" id="backer" />
                    <span className="buffer" id="buffer" />
                    <div className="playhead round vid-skin-bgcolor" id="playhead" />
                    <span className="progress vid-skin-bgcolor" id="progress" />
                  </div>
                </div>
              </div>
              <div id="bottom-row">
                <span id="left-control-group">
                  <span className="cell button">
                    <button type="button" className="vid-skin-bgcolor"
                            id="btn_playpause">Play</button>
                    <label id="play_rate" />
                  </span>
                  <span className="cell button">
                    <button type="button" className="vid-skin-bgcolor"
                            id="btn_reset">Reset</button>
                  </span>
                  <span className="cell button">
                      <div id="volume_slide_container">
                          <input id="rng_volumebar" name="volumeSlider"
                                 type="range" orient="vertical"
                                 className="align vid-skin-bgcolor adjust-volumebar"
                                 min="0" max="1" step="0.1"/>
                      </div>

                      <div
                          className="ptp-control ptp-volume-control ptp-control-bar-btn extend">
                          <button id="btn_volume" type="button" name="mute"
                                  className="ptp-control ptp-button-background ptp-btn-volume min-volume-state"
                                  title="Mute"/>
                          <input name="volumeSlider"
                                 className="ptp-input-slider ptp-control ptp-volume-slider ptp-volume-hidden"
                                 type="range" min="0" max="1" step="0.1"/>
                      </div>
                  </span>
                </span>
                <span id="right-control-group">
                  <span className="cell display vid-skin-fgcolor">
                    <span id="time">0:00</span> <span id="fwd-slash">/</span> <span
                      id="duration">0:00</span>
                  </span>
                  <span className="cell button">
                    <button
                      type="button" className="btn-captions vid-skin-bgcolor"
                      id="btn_captions">CC</button>
                  </span>
                  <span className="cell button">
                    <button type="button" className="btn-settings vid-skin-bgcolor" id="btn_settings">Settings</button>
                  </span>
                  <span className="cell button">
                    <button type="button" className="btn-fullscreen vid-skin-bgcolor" id="btn_fullscreen">FullScreen</button>
                  </span>
                </span>
              </div>
            </div>
            <div id="mainVideoDiv" className="main-video-div-style">
              <div id="videoDiv" className="video-div-style">
                <div id="buffering-overlay" className="overlay-spinner" hidden="hidden" />
              </div>

              <button id="adClickButton" type="button" className="clickableAd-button" hidden="hidden">Ad Click
                    </button>
              <div id="companionDiv" />
            </div>
          </div>
        );
    }
}

Player.displayName = 'Player';
Player.propTypes = {};
Player.defaultProps = {};

export default Player;
