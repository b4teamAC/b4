class ReferencePlayerUtils {

    static isBrowserIOSSafari() {
        return navigator.userAgent.indexOf('Safari') !== -1 && /(iPhone|iPad|iPod)/.test(navigator.userAgent);
    }

    /**
     * Format time for display.
     *
     * If a duration is given, then the time display is formatted to match
     * the duration. For example, if the time is 1 minute 23 seconds and the duration is 1 hour,
     * then the time is displayed as 00:01:23. However, if the duration is 30 minutes,
     * then the time is displayed as 01:23.
     *
     * @param time - in milliseconds
     * @param duration - total expected time, used to pad result
     * @returns {string} in format hours:mins:seconds
     */
    static formatDisplayTime(time, duration) {
        let value = Math.max(time, 0),
            hours = Math.floor(value / 1000 / 3600),
            mins = Math.floor((value / 1000) % 3600 / 60),
            secs = Math.floor((value / 1000) % 3600 % 60),
            dValue = 0, // duration
            dH = 0, // duration hours
            dM = 0; // duration minutes

        if (duration) {
            dValue = Math.max(duration, 0);
            dH = Math.floor(dValue / 1000 / 3600);
            dM = Math.floor((dValue / 1000) % 3600 / 60);
        }

        let result;
        result = hours === 0 ? (dH === 0 ? "" : "00:") : ((hours < 10 ? "0" : "") + hours.toString() + ":");
        result += mins === 0 ? ((dM > 0 || dH > 0 || hours > 0) ? "00:" : "") : ((mins < 10 ? "0" : "") + mins.toString() + ":");
        result += (secs < 10 ? "0" : "") + secs.toString();

        return result;
    }

    static isLive(player) {
        if (player) {
            let item = player.currentItem;
            if (item && item instanceof AdobePSDK.MediaPlayerItem) {
                return item.isLive;
            }
        }

        return false;
    }
}

export default ReferencePlayerUtils;

