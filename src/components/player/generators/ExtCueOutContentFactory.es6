import ExtCueOutOpportunityGenerator from "./ExtCueOutOpportunityGenerator";
import ServerMapOpportunityGenerator from "./ServerMapOpportunityGenerator";
//import PreRollOpportunityGenerator from './PreRollOpportunityGenerator';

class ExtCueOutContentFactory {
    constructor(audMetadata, fwMetadata, adPolicySelector) {
        this.audMetadata = audMetadata;
        this.fwMetadata = fwMetadata;
        this.adPolicySelector = adPolicySelector;
    }

    retrieveResolversCallbackFunc(item) {
        //for freewheel setup; always use fw resolver for both live and vod
        if (this.fwMetadata) {
            let result = [];
            result.push(new FreeWheelResolver(this.fwMetadata, item.isLive));
            return result;
        }
    }

    retrieveOpportunityGeneratorsCallbackFunc(item) {
        if (item.isLive || (item.config.adSignalingMode === AdobePSDK.AdSignalingMode.MANIFEST_CUES)) {
            //for live and when adSignalingMode is Manifest Cues, we use custom opportunity generator
            //for both flash and MSE
            let result = [];
            result.push(new ExtCueOutOpportunityGenerator(this.audMetadata));
            return result;
        } else {
            //for vod; if freewheel, use freewheel opportunity generator
            //for both flash and MSE
            if (this.fwMetadata) {
                let result = [];
                result.push(new FreeWheelAdSignalingModeOpportunityGenerator());
                return result;
            } else {
                //for default Aud + Vod, only for flash
                //we need to give custom generator
                //for MSE default will work
                if (item.key) {
                    let result = [];
                    result.push(new ServerMapOpportunityGenerator(this.audMetadata));
                    return result;
                }
            }
        }
    }

    retrieveAdPolicySelectorCallbackFunc(item) {
        if (this.adPolicySelector !== null &&
            this.adPolicySelector !== undefined) {
            console.log("let the custom AdPolicySelectors work");

            return this.adPolicySelector;
        }
        console.log("let the default AdPolicySelectors work");
    }

    retrieveCustomAdPlaybackHandlersCallbackFunc(item) {
        return null;
    }
}

export default ExtCueOutContentFactory;
