class ExtCueOutOpportunityGenerator {
    constructor(metadata) {
        this.metadata = metadata;
        this.debug = 0;
        this.adId = 1;

        this._item = null;
        this._client = null;
        this._adSignalingMode = null;
        this._advertisingMetadata = null;
        this._playbackTime = -1;
        this._processingTime = -1;
        this._skippedTimedMetadatas = [];
        this._processedTimedMetadatas = [];

        this.OPPORTUNITY_DURATION_KEY = "DURATION";
        this.OPPORTUNITY_ID_KEY = "BREAKID";
        this.OPP_TAG_NAME = "#EXT-X-CUE-OUT";
        this.MANIFEST_CUE_MODE = "manifest cues";

        this.compareTimedMetadata = function compare(a, b) {
            return a.time - b.time;
        };
    }

    configureCallbackFunc(item, client, mode, playhead, playbackRange) {
        this._item = item;
        this._client = client;
        this._adSignalingMode = mode;
        this._processedTimedMetadatas = [];

        let itemConfig = this._item.config;
        if (!itemConfig) {
            console.log("failed to get item config");
            return;
        }

        this._advertisingMetadata = itemConfig.advertisingMetadata;
        /*if (!this._advertisingMetadata) {
         console.log("#configure Unable to retrieve the advertising metadata.");
         return;
         }*/

        this._processingTime = playhead;
        this._playbackTime = playhead;

        //generate one pre-roll for live
        let placement = new AdobePSDK.Placement(AdobePSDK.PlacementType.PRE_ROLL,
            playhead,
            AdobePSDK.Placement.UNKNOWN_DURATION,
            AdobePSDK.PlacementMode.DEFAULT);

        let opportunity = new AdobePSDK.Opportunity("initial_opportunity", placement, this.metadata, null);
        client.resolve(opportunity);

        //now process mid-rolls
        let timedMetadataList = this._item.timedMetadata;
        if (this._item.isLive) {
            this.processMetadata(timedMetadataList, playbackRange);
        }
    }

    /**
     * Processes the specified metadata collection by iterating through it and
     * invoking process method on each metadata.
     *
     * We need to take care of two things:
     * - already processed timed metadata. They should not be processed again as it will
     *   lead to timeline conflicts
     * - skippable timed metadata. They should not be process immediately as they are placed
     *   in the past and processing them might increas the resolving time
     *
     * @param timedMetadata Collection of metadata to be processed.
     * @return SUCCESS if the operation was successful.
     */
    processMetadata(timedMetadataList, playbackRange) {
        // console.log("#processMetadata processing timedMetadatas.");
        if (this.debugOpp()) {
            let placementType = AdobePSDK.PlacementType.MID_ROLL; //"mid-roll";
            let placementMode = AdobePSDK.PlacementMode.DEFAULT; //"default";
            let placementTime = this._playbackTime + 240000;
            //let placementTime = playbackRange.end - 5000;
            let placementDuration = 12000;
            //let placementDuration = 900000;

            let placement = new AdobePSDK.Placement(placementType, placementTime, placementDuration, placementMode);
            let opportunity = new AdobePSDK.Opportunity("101", placement, null, null);
            this._client.resolve(opportunity);
            return;
        }
        if (!timedMetadataList)
            return;

        for (let i = 0; i < timedMetadataList.length; i++) {
            let timedMetadata = timedMetadataList[i];
            if (!this.isAlreadyProcessed(timedMetadata)) {
                if (this.isSkippable(timedMetadata)) {
                    // console.log("processMetadata Timed metadata skipped. ");
                    this._skippedTimedMetadatas.push(timedMetadata);
                } else {
                    // console.log("processMetadata Timed metadata processed.");
                    this._processedTimedMetadatas.push(timedMetadata);
                    if (this._processingTime < timedMetadata.time) {
                        this._processingTime = timedMetadata.time;
                    }

                    if (this.isPlacementOpportunity(timedMetadata)) {
                        let opportunity = this.createPlacementOpportunity(timedMetadata, this.metadata);
                        if (opportunity) {
                            // console.log("#process Placement opportunity created.");
                            this._client.resolve(opportunity);
                        }
                    }
                }
            }
        }

        this._skippedTimedMetadatas.sort(this.compareTimedMetadata);
        this._processedTimedMetadatas.sort(this.compareTimedMetadata);

        // console.log("#processMetadata Currently processed metadata : " + this._processedTimedMetadatas.length);
        // console.log("#processMetadata Currently skipped metadata : ", this._skippedTimedMetadatas.length);
    }

    /**
     * Checks if the specified timed metadata was already processed.
     * An timed metadata instance is considered to be processed if we tried to
     * create an opportunity from it, and not if we actually created an opportunity.
     *
     * @param timedMetadata Timed metadata to be checked.
     * @return true if the timed metadata was already processed and false otherwise.
     */
    isAlreadyProcessed(timedMetadata) {
        let len = this._processedTimedMetadatas.length;
        for (let i = 0; i < len; i++) {
            let tmp = this._processedTimedMetadatas[i];
            if ((tmp.time == timedMetadata.time) || (tmp.id === timedMetadata.id)) {
                return true;
            }
        }

        return false;
    }

    isSkippable(timedMetadata) {
        return (timedMetadata.time < this._processingTime);
    }

    isPlacementOpportunity(timedMetadata) {
        let validTag = timedMetadata.type == AdobePSDK.TimedMetadataType.TAG ||
            timedMetadata.type == "TAG";
        if (!validTag)
            return false;
        let tagName = timedMetadata.name;
        return tagName == this.OPP_TAG_NAME || tagName == "#EXT-X-CUE";
    }

    breakUpContent(content) {
        if (content.indexOf("#EXT") === 0) {
            let tagPivot = content.indexOf(':');
            if (tagPivot != -1) {
                content = content.substring(tagPivot + 1).trim();
            }
        }
        let arr = content.split(",");
        let tagMetadata = {};
        for (let i = 0; i < arr.length; i++) {
            let set = arr[i];
            let valMap = set.split("=");
            if (valMap.length >= 2) {
                let prop1 = valMap[1].replace(/['"]+/g, '');
                tagMetadata[valMap[0].toUpperCase()] = prop1;
            } else if (valMap.length == 1 && !Number.isNaN(parseFloat(valMap[0]))) {
                tagMetadata[this.OPPORTUNITY_DURATION_KEY] = valMap[0];
            }
        }
        return tagMetadata;
    }

    createPlacementOpportunity(timedMetadata, advertisingMetadata) {
        let placementType = AdobePSDK.PlacementType.MID_ROLL; //"mid-roll";
        let placementMode = AdobePSDK.PlacementMode.REPLACE; //"default";
        let placementTime = timedMetadata.time;
        let placementDuration = 0;

        let content = timedMetadata.content;
        if (!content)
            return;

        //we parse metadata again because flash sdk does not parse metadata properly for non-adobe cues
        let tagMetadata = this.breakUpContent(content);
        let id = null;

        if (timedMetadata.name == "#EXT-X-CUE") {
            if (tagMetadata["ELAPSED"])
                return;
            let durationProp = tagMetadata["DURATION"];
            placementDuration = durationProp * 1000;
            id = tagMetadata["ID"];
        } else if (timedMetadata.name == this.OPP_TAG_NAME) {
            if (tagMetadata[this.OPPORTUNITY_DURATION_KEY]) {
                let durationProp = tagMetadata[this.OPPORTUNITY_DURATION_KEY];
                placementDuration = durationProp * 1000;
            }

            if (!placementDuration) {
                placementDuration = 150000;
            }
            if (tagMetadata[this.OPPORTUNITY_ID_KEY]) {
                id = tagMetadata[this.OPPORTUNITY_ID_KEY];
            }
        }
        if (!id)
            id = this.adId++;

        if (!placementDuration || Number.isNaN(placementDuration)) {
            console.log("could not find duration in ad tag content. not able to generate placement opportunity");
            return;
        }

        let placement = new AdobePSDK.Placement(placementType, placementTime, placementDuration, placementMode);

        return new AdobePSDK.Opportunity(id, placement, advertisingMetadata, null);
    }

    debugOpp() {
        /*if(this.debug%5 == 0)
         return true;*/
        return false;
    }

    updateCallbackFunc(playhead, playbackRange) {

        this.debug++;
        // console.log("#update Playback range : " + playbackRange.begin + playbackRange.end);

        this.removeObsoleteMetadata(this._skippedTimedMetadatas, playbackRange);
        this.removeObsoleteMetadata(this._processedTimedMetadatas, playbackRange);

        let availableMetadata = [];
        if (playhead < this._playbackTime) {
            availableMetadata.push.apply(availableMetadata, this._skippedTimedMetadatas);
            this._skippedTimedMetadatas = [];
            this._processingTime = playhead;
        }

        let newTimedMetadata = this.retrieveNewTimedMetadata(this._item);
        if (newTimedMetadata.length) {
            availableMetadata.push.apply(availableMetadata, newTimedMetadata);
        }

        if (availableMetadata.length || this.debugOpp()) {
            availableMetadata.sort(this.compareTimedMetadata);
            // console.log("#processMetadata Timed metadata to be processed : ", availableMetadata.length);
            this._playbackTime = playhead;
            return this.processMetadata(availableMetadata, playbackRange);
        }

        //  console.log("#processMetadata No additional timed metadata to be processed.");
    }

    retrieveNewTimedMetadata(item) {
        let newTimedMetadata = [];
        let timedMetadataList = item.timedMetadata;
        if (timedMetadataList) {
            let length = timedMetadataList.length;
            if (length) {
                for (let i = length - 1; i >= 0; i--) {
                    let timedMetadata = timedMetadataList[i];
                    if (timedMetadata.time >= this._processingTime && !this.isAlreadyProcessed(timedMetadata)) {
                        newTimedMetadata.push(timedMetadata);
                    } else {
                        break;
                    }
                }
            }
        }
        return newTimedMetadata;
    }

    removeObsoleteMetadata(timedMetadataList, range) {
        for (let i = 0; i < timedMetadataList.length;) {
            let time = timedMetadataList[i].time;
            if (time >= range.begin) {
                return;
            }
            timedMetadataList.shift();
        }
    }

    cleanupCallbackFunc() {
    }
}

export default ExtCueOutOpportunityGenerator;
