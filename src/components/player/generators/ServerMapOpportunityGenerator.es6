class ServerMapOpportunityGenerator {
    constructor(metadata) {
        this.metadata = metadata;
    }

    configureCallbackFunc(item, client, mode, playhead, playbackRange) {
        let placement = new AdobePSDK.Placement(AdobePSDK.PlacementType.SERVER_MAP,
            AdobePSDK.Placement.UNKNOWN_POSITION,
            AdobePSDK.Placement.UNKNOWN_DURATION,
            AdobePSDK.PlacementMode.DEFAULT);

        let opportunity = new AdobePSDK.Opportunity(
            "initial_opportunity", placement, this.metadata, null);

        client.resolve(opportunity);
    }

    updateCallbackFunc(playhead, playbackRange) {
    }

    cleanupCallbackFunc() {
    }
}

export default ServerMapOpportunityGenerator;
