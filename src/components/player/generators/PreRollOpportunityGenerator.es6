class PreRollOpportunityGenerator {

    configureCallbackFunc(item, client, mode, playhead, playbackRange) {
        let placement = new AdobePSDK.Placement(AdobePSDK.PlacementType.PRE_ROLL,
            playhead,
            AdobePSDK.Placement.UNKNOWN_DURATION,
            AdobePSDK.PlacementMode.DEFAULT);

        let opportunity = new AdobePSDK.Opportunity("initial_opportunity", placement, item.resource.metadata, null);
        client.resolve(opportunity);
    }

    updateCallbackFunc(playhead, playbackRange) {
    }

    cleanupCallbackFunc() {
    }
}

export default PreRollOpportunityGenerator;
