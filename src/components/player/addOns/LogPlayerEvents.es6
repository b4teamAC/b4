import ReferencePlayerEvents from "../ReferencePlayerEvents";

// Player events logging: Seek bar, Ads, Caption, Buffering, Audio
class LogPlayerEvents {

    constructor(_playerWrapper) {
        this.playerWrapper = _playerWrapper;
    }

    enableEvents() {
        let player = this.playerWrapper.getPlayer();
        player.addEventListener(AdobePSDK.PSDKEventType.SEEK_BEGIN, this.onSeekStart.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.SEEK_END, this.onSeekComplete.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.SEEK_POSITION_ADJUSTED, this.onSeekPositionAdjusted.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.CAPTIONS_UPDATED, this.onCaptionsUpdate.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AUDIO_UPDATED, this.onAudioUpdate.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.BUFFERING_BEGIN, this.onBufferBegin.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.BUFFERING_END, this.onBufferEnd.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AD_PROGRESS, this.onAdProgress.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AD_BREAK_SKIPPED, this.onAdBreakSkipped.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AD_CLICK, this.onAdClick.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.PROFILE_CHANGED, this.onProfileChange.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.RATE_SELECTED, this.onPlaybackRateSelected.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.RATE_PLAYING, this.onPlaybackRatePlaying.bind(this));
    }

    onSeekStart(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "ReferencePlayer onSeekStart",
            params: {
                actualPosition: event.actualPosition,
                desiredPosition: event.desiredPosition
            }
        });
    }

    onSeekComplete(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "ReferencePlayer onSeekComplete",
            params: {
                actualPosition: event.actualPosition,
                desiredPosition: event.desiredPosition
            }
        });
    }

    onSeekPositionAdjusted(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "ReferencePlayer onSeekPositionAdjusted",
            params: {
                actualPosition: event.actualPosition,
                desiredPosition: event.desiredPosition
            }
        });
    }

    onCaptionsUpdate(event) {
        if (event && event.item) {
            //Bug PTPLAY-10071
            // These events are not getting fired in MSE workflows
            this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                message: "ReferencePlayer onCaptionsUpdate",
                params: event.item.closedCaptionsTracks
            });
        }
    }

    onAudioUpdate(event) {
        if (event && event.item) {
            this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                message: "ReferencePlayer onAudioUpdate",
                params: event.item.audioTracks
            });
        }
    }

    onBufferBegin(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "ReferencePlayer onBufferBegin",
            params: event
        });
    }

    onBufferEnd(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "ReferencePlayer onBufferEnd",
            params: event
        });
    }

    onAdProgress(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "onAdProgressEvent",
            params: event.progress
        });
    }

    onAdBreakSkipped() {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "onAdBreakSkippedEvent"
        });
    }

    onAdClick() {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "onAdClickEvent"
        });
    }

    onProfileChange(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "Profile Changed",
            params: event.profile
        });
    }

    onPlaybackRatePlaying(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "Playback Rate Playing",
            params: event.rate
        });
    }

    onPlaybackRateSelected(event) {
        this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
            message: "Playback Rate Selected",
            params: event.rate
        });
    }
}

export default LogPlayerEvents;
