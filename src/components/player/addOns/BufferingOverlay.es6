class BufferingOverlay {

    constructor(_player) {
        this.buffering = false;
        this.player = _player;
    }

    enableBufferingOverlay(videoDiv) {
        this.videoDiv = videoDiv;

        // Delay the buffering overlay display by 1 sec to avoid cases where buffering is almost immediate.
        this.showBufferingOverlay = (function () {
            if (this.buffering) {
                this.videoDiv.querySelector("#buffering-overlay").removeAttribute("hidden");
            } else {
                this.videoDiv.querySelector("#buffering-overlay").setAttribute("hidden", "true");
            }
        }).bind(this);

        let self = this;
        this.player.addEventListener(AdobePSDK.PSDKEventType.BUFFERING_BEGIN, function (event) {
            self.buffering = true;
            setTimeout(self.showBufferingOverlay, 1000);
        });

        this.player.addEventListener(AdobePSDK.PSDKEventType.BUFFERING_END, function (event) {
            self.buffering = false;
            self.showBufferingOverlay();
        });
        this.player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, function (event) {
            if ((event.status === AdobePSDK.MediaPlayerStatus.IDLE) ||
                (event.status === AdobePSDK.MediaPlayerStatus.ERROR)) {
                self.buffering = false;
                self.showBufferingOverlay();
            }
        });
    }
}

export default BufferingOverlay;
