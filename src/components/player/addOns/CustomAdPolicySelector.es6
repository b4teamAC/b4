class CustomAdPolicySelector {
    constructor(adWatchedPolicy, adBreakPolicy, adSeekPolicy) {
        this._adWatchedPolicy = adWatchedPolicy;
        this._adBreakPolicy = adBreakPolicy;
        this._adSeekPolicy = adSeekPolicy;
    }

    selectPolicyForAdBreakCallbackFunc(adPolicyInfo) {
        return this._adBreakPolicy;
    }

    selectWatchedPolicyForAdBreakCallbackFunc(adPolicyInfo) {
        return this._adWatchedPolicy;
    }

    selectPolicyForSeekIntoAdCallbackFunc(adPolicyInfo) {
        return this._adSeekPolicy;
    }

    selectAdBreaksToPlayCallbackFunc(adPolicyInfo) {
        let adBreakTimeLineItems = adPolicyInfo.adBreakTimelineItems,
            newAdBreakTimelineItems = [];
        if (adBreakTimeLineItems && adBreakTimeLineItems.length > 0) {
            let size = adBreakTimeLineItems.length;
            if (size > 0 && adPolicyInfo.currentTime <= adPolicyInfo.seekToTime) {
                let adBreakTimeLineItem = adBreakTimeLineItems[size - 1];
                let startTime = adBreakTimeLineItem.time / 1000;
                if (startTime < adPolicyInfo.seekToTime && !adBreakTimeLineItem.isWatched) {
                    newAdBreakTimelineItems.push(adBreakTimeLineItem);
                }
            }
        }
        return newAdBreakTimelineItems;
    }
}

export default CustomAdPolicySelector;
