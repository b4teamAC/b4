import ReferencePlayerEvents from "../ReferencePlayerEvents";

class ConsoleLogs {

    enableConsoleLogs(playerWrapper) {
        playerWrapper.addEventListener(ReferencePlayerEvents.LoadInitiatedEvent, this.onLoadInitEvent);
        playerWrapper.addEventListener(ReferencePlayerEvents.LogEvent, this.showLogEvent);
    }

    onLoadInitEvent(event) {
        console.log("Console log AddOn initialized...", event);
    }

    showLogEvent(event) {
        console.log("[Log] " + event.message, event.params);
    }
}

export default ConsoleLogs;
