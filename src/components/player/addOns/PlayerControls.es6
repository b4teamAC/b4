import SettingsManager from "./SettingsManager";
import ReferencePlayerEvents from "../ReferencePlayerEvents";
import ReferencePlayerUtils from "../ReferencePlayerUtils";

// This Add-On handles the media source list picked up from sources.js
// This need to be supplied the Ids of button, source list and text box where selected source is to be saved

class PlayerControls {
    constructor(videoControlDiv, videoDiv, playerWrapper) {
        this.settingsManager = null; // manager for settings GUI
        this.currentVolume = 0.1;
        this.seekBar = null;
        // playbackWaiting - to know if the player is currently seeking and
        // toggle button was pressed multiple times
        this.playbackWaiting = false;
        // toggleBtnPlayState - variable to know current status of player
        // according to togglePlayPause button
        this.toggleBtnPlayState = false;

        this.addOnVideoControls(videoControlDiv, videoDiv, playerWrapper);
    }

    addOnVideoControls(videoControlDiv, videoDiv, playerWrapper) {
        this.settingsManager = new SettingsManager();
        this.seekBar = new SeekBar();
        this.playerWrapper = playerWrapper;
        this.videoDiv = videoDiv;

        this.playerWrapper.addEventListener(ReferencePlayerEvents.LoadInitiatedEvent, this.onLoadInitEvent);
        let player = this.playerWrapper.getPlayer();
        player.addEventListener(AdobePSDK.PSDKEventType.TIME_CHANGED, this.onTimeChange.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.SEEK_POSITION_ADJUSTED, this.onSeekPositionAdjusted.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.RATE_SELECTED, this.onPlaybackRateSelected.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, this.onPlaybackRateChangeEvent.bind(this));

        // add full screen change event listeners to update fullscreen button icon
        // NOTE - event is not triggered if user enables fullscreen from hotkey (F11)
        document.addEventListener("webkitfullscreenchange", this.onFullscreenChange, false);
        document.addEventListener("fullscreenchange", this.onFullscreenChange, false);
        document.addEventListener("mozfullscreenchange", this.onFullscreenChange, false);
        document.addEventListener("MSFullscreenChange", this.onFullscreenChange, false);

        // Settings button
        let btnSettings = this.videoDiv.querySelector("#btn_settings");
        btnSettings.classList.add("invisible");
        btnSettings.addEventListener("click", function () {
            if (this.settingsManager.toggleDisplay().visible) {
                btnSettings.classList.add("on");
            } else {
                btnSettings.classList.remove("on");
            }
        }.bind(this));

        // volume button
        let btnVolume = this.videoDiv.querySelector("#btn_volume");
        btnVolume.addEventListener("mouseover", function () {
            this.videoDiv.querySelector("#volume_slide_container").classList.remove("hidden");
        }.bind(this));
        btnVolume.addEventListener("mouseout", function () {
            this.videoDiv.querySelector("#volume_slide_container").classList.add("hidden");
        }.bind(this));

        let rngVolume = this.videoDiv.querySelector("#rng_volumebar");
        rngVolume.onchange = (function () {
            // update mute button style
            this.toggleMuteButtonStyle(rngVolume.value === 0);
            this.setVolume(rngVolume.value);
        }.bind(this));
        // Set initial UI volume
        rngVolume.value = this.currentVolume;
        this.toggleMuteButtonStyle(rngVolume.value === 0);

        let volumeContainer = this.videoDiv.querySelector("#volume_slide_container");
        volumeContainer.addEventListener("mouseover", function () {
            this.classList.remove("hidden");
        });
        volumeContainer.addEventListener("mouseout", function () {
            this.classList.add("hidden");
        });

        // full screen button
        let btnFullScreen = this.videoDiv.querySelector("#btn_fullscreen");
        btnFullScreen.addEventListener("click", function () {
            this.toggleFullScreenButtonStyle();
        }.bind(this));

        // set event handler for UI elements
        let btnPlayPause = this.videoDiv.querySelector("#btn_playpause");
        btnPlayPause.addEventListener("click", function () {
            this.togglePlayPause();
        }.bind(this));

        let btnReset = this.videoDiv.querySelector("#btn_reset");
        btnReset.addEventListener("click", this.resetBtnClick.bind(this));

        btnVolume.addEventListener("click", function () {
            let result = this.toggleMute();
            this.toggleMuteButtonStyle(result.mute);
            this.videoDiv.querySelector("#rng_volumebar").value = result.volume;
        }.bind(this));

        // initialize the seek bar
        this.seekBar.init(videoControlDiv);
        this.seekBar.setPositionChangeListener(function (pos) {
            let range = this.playerWrapper.getPlaybackRange();
            if (range && range instanceof AdobePSDK.TimeRange) {

                let duration = range.duration;
                if (!isFinite(duration)) {
                    return;
                }

                // The timeline shows localtime, therefore we need to map this timeto virtual time
                let timeline = this.playerWrapper.getPlayer().timeline,
                    localBegin = timeline.convertToLocalTime(range.begin),
                    localEnd = timeline.convertToLocalTime(range.end),
                    localDuration = localEnd - localBegin,
                    localSeekTime = localDuration * pos; // seek bar range is [0,1]

                // convert to virtual time
                let virtualSeekTime = timeline.convertToVirtualTime(localSeekTime);

                //Per API spec position should be absolute position and not relative position
                virtualSeekTime = range.begin + virtualSeekTime;
                this.playerWrapper.seek(virtualSeekTime);

                this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "[PlayerControls] seek to " + localSeekTime + " / " + localDuration
                });
            }

        }.bind(this));
    }

    removeEventListeners() {
        this.playerWrapper.removeEventListener(ReferencePlayerEvents.LoadInitiatedEvent, this.onLoadInitEvent);
        let player = this.playerWrapper.getPlayer();
        player.removeEventListener(AdobePSDK.PSDKEventType.TIME_CHANGED, this.onTimeChange);
        player.removeEventListener(AdobePSDK.PSDKEventType.SEEK_POSITION_ADJUSTED, this.onSeekPositionAdjusted);
        player.removeEventListener(AdobePSDK.PSDKEventType.RATE_SELECTED, this.onPlaybackRateSelected);
        player.removeEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, this.onPlaybackRateChangeEvent);

        document.removeEventListener("webkitfullscreenchange", this.onFullscreenChange, false);
        document.removeEventListener("fullscreenchange", this.onFullscreenChange, false);
        document.removeEventListener("mozfullscreenchange", this.onFullscreenChange, false);
        document.removeEventListener("MSFullscreenChange", this.onFullscreenChange, false);
    }

    /**
     * Toggles UI style for fullscreen when fullscreen mode is enabled/disabled.
     */
    toggleFullScreenButtonStyle() {
        let isFullscreen = this.toggleFullScreen().fullscreen;
        if (isFullscreen) {
            this.videoDiv.querySelector("#btn_fullscreen").classList.add("fullscreen-state");
        } else {
            this.videoDiv.querySelector("#btn_fullscreen").classList.remove("fullscreen-state");
        }
    }

    /**
     * Toggles UI style for volume/mute when volume is muted/unmuted.
     * @param isMute {boolean}
     */
    toggleMuteButtonStyle(isMute) {
        if (isMute) {
            this.videoDiv.querySelector("#btn_volume").classList.add("mute-state");
        } else {
            this.videoDiv.querySelector("#btn_volume").classList.remove("mute-state");
        }
    }

    /**
     * Update controls UI when full screen change is detected.
     */
    onFullscreenChange(event) {
        if (this.isFullScreen()) {
            this.querySelector("#btn_fullscreen").classList.add("fullscreen-state");
            this.classList.remove("videoDivMaxSize");
        } else {
            this.querySelector("#btn_fullscreen").classList.remove("fullscreen-state");
            this.classList.add("videoDivMaxSize");
        }

        // reset seekbar to readjust positions based on new screen size
        this.seekBar.readjustSeekBar();
    }

    /**
     * Called when the playerWrapper's time changes.
     * Update UI to show current time, seekbar playhead position and buffer level.
     */
    onTimeChange(event) {
        if (!this.seekBar.isSeeking) {
            let time = event.time,
                range = event.target.playbackRange;

            if (range && range instanceof AdobePSDK.TimeRange) {
                let timeline = this.playerWrapper.getPlayer().timeline,
                    timeWithoutAds = timeline.convertToLocalTime(time),
                    localBegin = timeline.convertToLocalTime(range.begin),
                    localEnd = timeline.convertToLocalTime(range.end),
                    rangeWithoutAds = new AdobePSDK.TimeRange(localBegin, localEnd - localBegin);

                this.seekBar.updatePlayhead(timeWithoutAds, rangeWithoutAds);
                let adjustedTime = timeWithoutAds - rangeWithoutAds.begin;
                this.updateDisplayTime(this.playerWrapper.getVideoDiv(), adjustedTime, rangeWithoutAds);

                let bufferedRange = this.playerWrapper.getBufferedRange(),
                    reverseTrickPlay = !!(this.isTrickPlayEnabled() &&
                    this.getTrickPlayRate() && this.getTrickPlayRate() < 0);
                this.seekBar.updateBufferLevel(timeline.convertToLocalTime(bufferedRange.end),
                    rangeWithoutAds, reverseTrickPlay);
            }
        }
    }

    /**
     * If the seek position was adjusted, re-position the playhead.
     */
    onSeekPositionAdjusted(event) {
        let timeline = this.playerWrapper.getPlayer().timeline,
            range = this.playerWrapper.getPlaybackRange(),
            timeWithoutAds = timeline.convertToLocalTime(event.actualPosition),
            localBegin = timeline.convertToLocalTime(range.begin),
            localEnd = timeline.convertToLocalTime(range.end),
            rangeWithoutAds = new AdobePSDK.TimeRange(localBegin, localEnd - localBegin);

        this.seekBar.updatePlayhead(timeWithoutAds, rangeWithoutAds);
        this.updateDisplayTime(this.videoDiv, timeWithoutAds, rangeWithoutAds);
    }

    /**
     * Set the displayed media time and duration
     */
    updateDisplayTime(videoDiv, time, range) {
        let timeElm = videoDiv.querySelector("#time"),
            durationElm = videoDiv.querySelector("#duration"),
            fwdSlashElm = videoDiv.querySelector("#fwd-slash");

        if (ReferencePlayerUtils.isLive(this.playerWrapper.getPlayer())) {
            timeElm.style.visibility = "hidden";
            fwdSlashElm.style.visibility = "hidden";
            // set Live button if not already set
            if (durationElm.getElementsByTagName("button").length == 0) {
                // remove child elements
                while (durationElm.firstChild) {
                    durationElm.removeChild(durationElm.firstChild);
                }

                durationElm.appendChild(liveLink);
            }
        } else {
            timeElm.innerHTML = ReferencePlayerUtils.formatDisplayTime(time, range.duration);
            timeElm.style.visibility = "visible";
            fwdSlashElm.style.visibility = "visible";
            let roundedValue = Math.round(range.duration / 1000) * 1000;
            durationElm.innerHTML = ReferencePlayerUtils.formatDisplayTime(roundedValue, null);
        }
    }

    onLoadInitEvent(event) {
        // hide settings button
        this.videoDiv.querySelector("#btn_settings").classList.add("invisible");
        // hide captions button
        this.videoDiv.querySelector("#btn_captions").classList.add("invisible");
    }

    /**
     * Toggle in and out of fullscreen modes.
     *
     * @returns {{fullscreen: boolean}} - true if moved to fullscreen, false if moved out of fullscreen
     */
    toggleFullScreen() {
        let view = this.playerWrapper.getPlayer().view;
        if (typeof view !== "undefined") {
            if (view.isFullScreen()) {
                view.exitFullScreen();
                return {
                    fullscreen: false
                };
            } else {
                view.makeFullScreen();
                return {
                    fullscreen: true
                };
            }
        }
    }

    /**
     * Check if player is currently in fullscreen mode.
     * @returns {boolean}
     */
    isFullScreen() {
        return (typeof this.getPlayer().view == "object") ? this.getPlayer().view.isFullScreen() : false;
    }

    /**
     * Toggle volume between mute and previous volume levels.
     * @returns {{mute: boolean}} - true if volume is now muted, false if volume is now unmuted.
     */
    toggleMute() {
        if (this.playerWrapper.getPlayer().volume == 0) {
            this.currentVolume = this.unmute();
            return {
                mute: false,
                volume: this.currentVolume
            };
        } else {
            this.mute();
            return {
                mute: true,
                volume: 0
            };
        }
    }

    /**
     * Set media player's volume.
     * @param volume - [0,1]
     */
    setVolume(volume) {
        let result = this.playerWrapper.getPlayer().volume = volume;
        if (result > 0) {
            this.currentVolume = volume;
        }
    }

    /**
     * Sets media player's volume to 0.
     */
    mute() {
        this.playerWrapper.getPlayer().volume = 0;
    }

    /**
     * Restores media player's volume to level before being muted.
     * @returns {number}
     */
    unmute() {
        this.playerWrapper.getPlayer().volume = this.currentVolume;
        return this.currentVolume;
    }

    /**
     * Reset play to beginning.
     */
    resetBtnClick() {
        if (this.playerWrapper.areControlsDisabledInAd()) {
            return;
        }

        this.playerWrapper.seekToLocal(0);
    }

    onPlaybackRateSelected(event) {
        this.onPlaybackRateChange(event.rate, false);
    }

    onPlaybackRateChangeEvent(event) {
        switch (event.status) {
            case AdobePSDK.MediaPlayerStatus.PLAYING:
                this.onPlaybackRateChange(this.playerWrapper.getPlayer().rate, true);
                break;
            case AdobePSDK.MediaPlayerStatus.PAUSED:
                this.onPlaybackRateChange(0, true);
                break;
            case AdobePSDK.MediaPlayerStatus.COMPLETE:
                this.onPlaybackRateChange(-1, true); // use -1 to denote playback is complete
                break;
        }
    }

    getTrickPlayRate() {
        let rate = this.playerWrapper.getPlayer().rate;
        if (rate < 0 || rate > 1 || (rate < 1 && rate !== 0)) {
            return rate;
        }

        return false;
    }

    isTrickPlayEnabled() {
        let enabled = false;

        /*
         if (TrickPlay !== null)
         enabled = TrickPlayEnabled;
         */

        return enabled;
    }

    /**
     * Update the Play/Pause/Replay button with the correct icon based on the playback rate.
     * statusChanged variable introduced to distinguish between whether playback rate was
     * called by PlaybackRateSelectedEvent or StatusChangeEvent
     */
    onPlaybackRateChange(rate, statusChanged) {
        let btnPlayPause = this.videoDiv.querySelector("#btn_playpause"),
            outRate = this.videoDiv.querySelector("#play_rate");

        switch (rate) {
            case -1: // complete
                btnPlayPause.innerHTML = "Replay";
                btnPlayPause.classList.remove("pause-state");
                btnPlayPause.classList.add("replay-state");
                outRate.innerHTML = "";
                break;
            case 0: // paused
                btnPlayPause.innerHTML = "Play";
                btnPlayPause.classList.remove("pause-state");
                btnPlayPause.classList.remove("replay-state");
                outRate.innerHTML = "";
                break;
            case 1: // playing
                if (this.playerWrapper.getPlayer().status === AdobePSDK.MediaPlayerStatus.PREPARED) {
                    break;
                }
                //Logic for if togglePlayPause pressed multiple times while seeking,
                // it should continue with the option last pressed
                if (this.playbackWaiting && statusChanged) {
                    this.playbackWaiting = false;
                    let result = this.toggleBtnPlayState ? this.play() : this.pause();
                    break;
                }
                btnPlayPause.innerHTML = "Pause";
                btnPlayPause.classList.add("pause-state");
                btnPlayPause.classList.remove("replay-state");
                outRate.innerHTML = "";
                break;
            default: // playing with trick mode
                btnPlayPause.innerHTML = "Play";
                btnPlayPause.classList.remove("pause-state");
                btnPlayPause.classList.remove("replay-state");
                outRate.innerHTML = rate.toString() + "x";
                break;
        }
    }

    play() {
        switch (this.playerWrapper.getPlayer().status) {
            case AdobePSDK.MediaPlayerStatus.COMPLETE:
                this.playerWrapper.loadPrevMediaResource();
                break;
            case AdobePSDK.MediaPlayerStatus.PREPARED:
            case AdobePSDK.MediaPlayerStatus.PAUSED:
            case AdobePSDK.MediaPlayerStatus.SEEKING:
                this.playerWrapper.getPlayer().play();
                break;

            case AdobePSDK.MediaPlayerStatus.PLAYING:
                if (this.isTrickPlayEnabled() && this.getTrickPlayRate()) {
                    // cancel trick play and return to playing
                    //player.play();
                    this.playerWrapper.getPlayer().rate = 1;
                }
                break;
        }
    }

    pause() {
        switch (this.playerWrapper.getPlayer().status) {
            case AdobePSDK.MediaPlayerStatus.PREPARED:
            case AdobePSDK.MediaPlayerStatus.PLAYING:
            case AdobePSDK.MediaPlayerStatus.COMPLETE:
            case AdobePSDK.MediaPlayerStatus.SEEKING:
                this.playerWrapper.getPlayer().pause();
                break;
        }
    }

    /**
     * Toggle between playing and paused states. If player is performing trick play, returns
     * player to PLAYING state. If playback has completed, restarts playback from start of video.
     *
     * @returns {{playing: boolean}} - true if player is playing, false if player is paused
     */
    togglePlayPause() {
        switch (this.playerWrapper.getPlayer().status) {
            case AdobePSDK.MediaPlayerStatus.PAUSED:
                // For iOS devices only,
                // Whenever user enters fullscreen mode, native controls take over.
                // When the user exits fullscreen, the toggleBtnPlayState variable remains
                // in an invalid state, since it is only updated via Reference Player controls,
                // not via the video tag. So,  we ignore this variable in case of iOS.
                let iOSRegex = /(iPhone|iPad|iPod)/;
                if (this.toggleBtnPlayState == true && !(navigator.userAgent.indexOf('Safari') !== -1 &&
                    iOSRegex.test(navigator.userAgent))) {
                    this.playbackWaiting = true;
                    btnPlayPause.innerHTML = "Play";
                    btnPlayPause.classList.remove("pause-state");
                    btnPlayPause.classList.remove("replay-state");
                    this.pause();
                    this.toggleBtnPlayState = false;
                    break;
                }
            case AdobePSDK.MediaPlayerStatus.PREPARED:
                this.play();
                this.toggleBtnPlayState = true;
                break;
            case AdobePSDK.MediaPlayerStatus.COMPLETE:
                let ID3 = ReferencePlayer.AddOns.ID3();
                ID3.cleanup();
                this.play();
                this.toggleBtnPlayState = true;
                break;

            case AdobePSDK.MediaPlayerStatus.SEEKING:
                if (btnPlayPause.innerHTML == "Play") {
                    this.play();
                    this.toggleBtnPlayState = true;
                } else {
                    this.pause();
                    this.toggleBtnPlayState = false;
                }
                break;
            case AdobePSDK.MediaPlayerStatus.PLAYING:
                if (this.isTrickPlayEnabled() && this.getTrickPlayRate()) {
                    this.play();
                    this.toggleBtnPlayState = true;
                } else {
                    this.pause();
                    this.toggleBtnPlayState = false;
                }
                break;

            default:
                this.playerWrapper.dispatchEvent(ReferencePlayerEvents.LogEvent, {
                    message: "ReferencePlayer::togglePlayPause - Player is in an invalid state for play/pause."
                });
        }
        return {
            playing: this.toggleBtnPlayState
        };
    }
}

/**
 * Seekbar object which handles the display of the playerWrapper progress, playhead, and buffer level.
 */
class SeekBar {

    constructor() {
        this.playheadHalfSize = 8; // must be half width of playhead defined in CSS
        this.containerElm = null;
        this.playheadElm = null;
        this.backerElm = null;
        this.bufferElm = null;
        this.progressElm = null;
        this.currentPos = 0; // [0,1] relative to range
        this.currentBuffer = 0; // [0,1] relative to range
        this.listener = null;
        this.isSeeking = false;
    }

    getSeekBarBounds() {
        return this.backerElm.getBoundingClientRect();
    }

    onPositionChange(event) {
        let pos = event.pageX || event.clientX,
            bounds = this.getSeekBarBounds();
        // normalize position to seek bounds
        pos -= bounds.left;
        let percent = pos / (bounds.right - bounds.left);

        // check bounds
        if (percent > 1) percent = 1;
        else if (percent < 0) percent = 0;

        this.currentPos = percent;
        this.setPosition(percent);
    }

    setPosition(percent) {
        // adjust to keep playhead graphic inside container on right side
        let bounds = this.getSeekBarBounds(),
            pos = percent * (bounds.right - bounds.left);

        this.progressElm.style.width = (pos).toString() + "px";
        this.playheadElm.style.left = (pos - this.playheadHalfSize).toString() + "px";

        this.currentPos = percent;
        return percent;
    }

    setBufferLevel(percent) {
        this.currentBuffer = percent;
        let bounds = this.getSeekBarBounds(),
            pos = percent * (bounds.right - bounds.left);

        this.bufferElm.style.width = pos.toString() + "px";
    }

    convertTimeToRelativePosition(time, range) {
        // convert time to a percentage of the duration
        let position = (time - range.begin) / range.duration;
        if (position > 1) position = 1;
        else if (position < 0) position = 0;
        return position;
    }

    unattachEvents(event) {
        this.removeEventListener('mousemove', this.onPositionChange);
        this.removeEventListener('mouseout', this.unattachEvents);
        this.removeEventListener('mouseup', this.unattachEvents);

        this.addEventListener('mouseover', this.attachEvents);
        this.isSeeking = false;
    }

    attachEvents(event) {
        this.addEventListener('mousemove', this.onPositionChange);
        this.addEventListener('mouseout', this.unattachEvents);
        this.addEventListener('mouseup', this.unattachEvents);
        this.isSeeking = true;
        return false;
    }

    init(videoControlDiv) {
        this.containerElm = videoControlDiv.querySelector("#seekbar");
        this.backerElm = videoControlDiv.querySelector("#backer");
        this.playheadElm = videoControlDiv.querySelector("#playhead");
        this.bufferElm = videoControlDiv.querySelector("#buffer");
        this.progressElm = videoControlDiv.querySelector("#progress");

        // client seek via scrubbing
        this.containerElm.addEventListener('mousedown', this.attachEvents);

        // client seek via single click or 'mouseup' after scrubbing
        this.containerElm.addEventListener('click', function (e) {
            if (this.listener !== null) {
                this.onPositionChange(e);
                // callback to notify of desired seek position
                this.listener.call(this, this.currentPos);
            }
        }.bind(this));

        // end client seek via scrubbing
        document.addEventListener('mouseup', function (e) {
            this.containerElm.removeEventListener('mouseover', this.attachEvents);
        }.bind(this));
    }

    /**
     * Change the position of the playhead.
     * @param time - time in seconds to position playhead
     * @param range - current playback range
     */
    updatePlayhead(time, range) {
        if (!this.isSeeking) {
            let pos = this.convertTimeToRelativePosition(time, range);
            this.setPosition(pos);
        }
    }

    /**
     * Change the position of the buffer range.
     * @param time - time in seconds to position head of buffer
     * @param range - current playback range
     */
    updateBufferLevel(level, range, reverseTrickPlay) {
        if (!this.isSeeking) {
            let position = reverseTrickPlay ? 0 : this.convertTimeToRelativePosition(level, range);
            this.setBufferLevel(position);
        }
    }

    /**
     * Set listener which is called when the playhead position changes by the user event.
     * Listener is not called when playhead position changes via updatePlayhead function.
     * @param listener - callback function, passed single position argument representing the
     * playhead position in the range [0,1].
     */
    setPositionChangeListener(funct) {
        this.listener = funct;
    }

    /**
     * Readjust position levels if seekbar is resized.
     * Should be called every time the video window changes size.
     */
    readjustSeekBar() {
        this.setPosition(this.currentPos);
        this.setBufferLevel(this.currentBuffer);
    }
}

export default PlayerControls;
