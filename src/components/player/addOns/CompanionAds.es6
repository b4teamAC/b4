class CompanionAds {
    constructor() {
        this.companionDiv = document.getElementById("companionDiv");
    }

    enable(playerWrapper) {
        /**
         * This function displays the companion ads for a given ad.
         */
        let displayCompanions = function (ad) {
            if (ad) {
                let companions = ad.companionAssets,
                    divTag;
                if (!companions) {
                    return;
                }
                for (let i = 0; i < companions.length; ++i) {
                    let bannerId = "banner" + companions[i].width + "x" + companions[i].height;
                    divTag = this.companionDiv.querySelector("#" + bannerId);

                    if (!divTag) {
                        divTag = document.createElement("div");
                        divTag.style.width = companions[i].width + "px";
                        divTag.style.height = companions[i].height + "px";
                        divTag.setAttribute("id", bannerId);
                        this.companionDiv.appendChild(divTag);
                    }
                    divTag.innerHTML = companions[i].bannerData;
                }
            }
        };

        // Register 'displayCompanions' for the AdStartedEvent.
        playerWrapper.getPlayer().addEventListener(AdobePSDK.PSDKEventType.AD_STARTED, function (event) {
            displayCompanions(event.ad);
        });
    }

    reset() {
        while (this.companionDiv.hasChildNodes()) {
            this.companionDiv.removeChild(this.companionDiv.lastChild);
        }
    }
}

export default CompanionAds;
