// QoS
import ReferencePlayerEvents from "../ReferencePlayerEvents";

class QoS {

    constructor() {
        this.qosProvider = null,
            this.isInitialTime = true;
    }

    enableQoSMetrics(_playerWrapper) {
        this.playerWrapper = _playerWrapper;
        this.qosProvider = new AdobePSDK.QOSProvider();
        this.qosProvider.attachMediaPlayer(this.playerWrapper.getPlayer());
    }

    onLoadInitEvent(event) {
        console.log("QoS AddOn initialized", event);
        this.isInitialTime = true;
    }

    start() {
        // register for events
        this.playerWrapper.addEventListener(ReferencePlayerEvents.LoadInitiatedEvent, this.onLoadInitEvent);
    }

    stop() {
        //qosProvider.detachMediaPlayer();
        this.playerWrapper.removeEventListener(ReferencePlayerEvents.LoadInitiatedEvent, this.onLoadInitEvent);
    }

    static getDate() {
        let dd = new Date();
        return dd.getFullYear() + "-" + dd.getMonth() + "-" + dd.getDate() + " " + dd.getHours() + ":" + dd.getMinutes() + ":" + dd.getSeconds() + "." + dd.getMilliseconds();
    }

    getCurrentMetrics() {
        let metrics = this.qosProvider.playbackInformation;
        if (!metrics ||
            metrics === AdobePSDK.PSDKErrorCode.ILLEGAL_STATE) {
            return;
        }

        let letsLog = "[" + QoS.getDate() + "]";

        if (metrics) {
            letsLog += " [timeToFirstByte:" + (metrics.timeToFirstByte).toFixed(2) + "]";
            letsLog += " [timeToLoad:" + (metrics.timeToLoad).toFixed(2) + "]";
            letsLog += " [timeToStart:" + (metrics.timeToStart).toFixed(2) + "]";
            letsLog += " [timeToFail:" + (metrics.timeToFail).toFixed(2) + "]";
            letsLog += " [totalSecondsPlayed:" + (metrics.totalSecondsPlayed).toFixed(2) + "]";
            letsLog += " [totalSecondsSpent:" + (metrics.totalSecondsSpent).toFixed(2) + "]";
            letsLog += " [frameRate:" + (metrics.frameRate).toFixed(2) + "]";
            letsLog += " [droppedFrameCount:" + (metrics.droppedFrameCount ? (metrics.droppedFrameCount).toFixed(2) : 0) + "]";
            letsLog += " [perceivedBandwidth:" + (metrics.perceivedBandwidth).toFixed(2) + "]";
            letsLog += " [emptyBufferCount:" + metrics.emptyBufferCount + "]";
            letsLog += "\n";
        }
        let playbackMetrics = this.playerWrapper.getPlayer().playbackMetrics;
        if (typeof playbackMetrics === 'undefined' ||
            playbackMetrics === null ||
            playbackMetrics === AdobePSDK.PSDKErrorCode.ILLEGAL_STATE ||
            playbackMetrics === AdobePSDK.PSDKErrorCode.ELEMENT_NOT_FOUND) {
            playbackMetrics = {
                droppedFramesCount: 0,
                perceivedBandwidth: 0
            };
        }


        if (metrics && playbackMetrics) {
            let audioVideoMetrics = playbackMetrics;
            //for video
            let videoBitrateValue, videoPerceivedBandwidthValue, videoBufferLengthValue, videoDroppedFramesCountValue;
            if (typeof audioVideoMetrics.bitRate === 'object') //MSE + MediaElements
            {
                videoBitrateValue = audioVideoMetrics.bitRate.video ? audioVideoMetrics.bitRate.video : 0;
                videoPerceivedBandwidthValue = audioVideoMetrics.perceivedBandwidth.video ? audioVideoMetrics.perceivedBandwidth.video : 0;
                videoBufferLengthValue = audioVideoMetrics.bufferLength.video ? audioVideoMetrics.bufferLength.video : 0;
                videoDroppedFramesCountValue = audioVideoMetrics.droppedFramesCount.video ? audioVideoMetrics.droppedFramesCount.video : 0;
            } else //flash fallback
            {
                videoBitrateValue = audioVideoMetrics.bitRate ? audioVideoMetrics.bitRate : 0;
                videoPerceivedBandwidthValue = audioVideoMetrics.perceivedBandwidth ? audioVideoMetrics.perceivedBandwidth : 0;
                videoBufferLengthValue = audioVideoMetrics.bufferLength ? audioVideoMetrics.bufferLength : 0;
                videoDroppedFramesCountValue = audioVideoMetrics.droppedFramesCount ? audioVideoMetrics.droppedFramesCount : 0;
            }

            letsLog += " [video-bitrate-index:" + videoBitrateValue + "]";
            letsLog += " [video-bandwidth:" + videoPerceivedBandwidthValue + "]";
            letsLog += " [video-bufferLength:" + videoBufferLengthValue + "]";
            letsLog += " [video-droppedFrames:" + videoDroppedFramesCountValue + "]";

            //for audio
            let audioBitrateValue, audioPerceivedBandwidthValue, audioBufferLengthValue, audioDroppedFramesCountValue;
            if (typeof audioVideoMetrics.bitRate === 'object') //MSE + MediaElements
            {
                audioBitrateValue = audioVideoMetrics.bitRate.audio ? audioVideoMetrics.bitRate.audio : 0;
                audioPerceivedBandwidthValue = audioVideoMetrics.perceivedBandwidth.audio ? audioVideoMetrics.perceivedBandwidth.audio : 0;
                audioBufferLengthValue = audioVideoMetrics.bufferLength.audio ? audioVideoMetrics.bufferLength.audio : 0;
                audioDroppedFramesCountValue = audioVideoMetrics.droppedFramesCount.audio ? audioVideoMetrics.droppedFramesCount.audio : 0;
            } else //flash fallback
            {
                audioBitrateValue = audioVideoMetrics.bitRate ? audioVideoMetrics.bitRate : 0;
                audioPerceivedBandwidthValue = audioVideoMetrics.perceivedBandwidth ? audioVideoMetrics.perceivedBandwidth : 0;
                audioBufferLengthValue = audioVideoMetrics.bufferLength ? audioVideoMetrics.bufferLength : 0;
                audioDroppedFramesCountValue = audioVideoMetrics.droppedFramesCount ? audioVideoMetrics.droppedFramesCount : 0;
            }

            letsLog += " [audio-bitrate-index:" + audioBitrateValue + "]";
            letsLog += " [audio-bandwidth:" + audioPerceivedBandwidthValue + "]";
            letsLog += " [audio-bufferLength:" + audioBufferLengthValue + "]";
            letsLog += " [audio-droppedFrames:" + audioDroppedFramesCountValue + "]";
            letsLog += "\n";
        }

        this.playerWrapper.dispatchEventWrapper(ReferencePlayerEvents.LogEvent, {
            message: letsLog
        });
    }
}

export default QoS;


