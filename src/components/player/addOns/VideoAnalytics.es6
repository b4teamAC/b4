class VideoAnalytics {

    constructor() {
        this.configuration = {
            player: {
                name: 'TVSDK-HTML',
                version: '1.0.0'
            },

            visitor: {
                marketing_cloud_org_id: '3ce342c75100435b0a490d4c@adobeorg',
                tracking_server: 'obumobile1.sc.omtrdc.net'
            },

            app_measurement: {
                tracking_server: 'obumobile1.sc.omtrdc.net',
                account: 'sample-account',
                page_name: 'sample page name'
            },

            va: {
                tracking_server: 'obumobile1.hb.omtrdc.net',
                publisher: '3ce342c75100435b0a490d4c@adobeorg',
                channel: 'test-channel'
            }
        };

        this.videoAnalyticsTracker = null;
    }

    enableVideoAnalytics(mediaPlayer, contentInfo) {
        // Video Analytics
        if (this.videoAnalyticsTracker) {
            this.videoAnalyticsTracker.detachMediaPlayer();
        }

        //metadata.setMetadata(AdobePSDK.MetadataKeys.VIDEO_ANALYTICS_METADATA_KEY, getVideoAnalyticsMetadata(contentInfo));

        this.videoAnalyticsTracker = new AdobePSDK.VA.VideoAnalyticsProvider(
            this.getVideoAnalyticsMetadata(contentInfo),
            this.getNielsenTrackerExtension());
        this.videoAnalyticsTracker.attachMediaPlayer(mediaPlayer);
    }

    getVideoAnalyticsMetadata(contentInfo) {
        // Set-up the Visitor ID component.
        let visitor = new Visitor(this.configuration.visitor.marketing_cloud_org_id);
        visitor.trackingServer = this.configuration.visitor.tracking_server;

        // Set-up the AppMeasurement component.
        let appMeasurement = new AppMeasurement();
        appMeasurement.visitor = visitor;
        appMeasurement.trackingServer = this.configuration.app_measurement.tracking_server;
        appMeasurement.account = this.configuration.app_measurement.account;
        appMeasurement.pageName = this.configuration.app_measurement.page_name;
        appMeasurement.charSet = "UTF-8";
        appMeasurement.visitorID = contentInfo.visitorId;

        // Set-up VA Tracking Metadata.
        let vaObj = new AdobePSDK.VA.VideoAnalyticsMetadata();
        vaObj.appMeasurement = appMeasurement;
        vaObj.trackingServer = this.configuration.va.tracking_server;
        vaObj.publisher = this.configuration.va.publisher;
        vaObj.channel = this.configuration.va.channel;
        vaObj.playerName = this.configuration.player.name;
        vaObj.appVersion = this.configuration.player.version;
        vaObj.videoName = contentInfo.title;
        vaObj.videoId = contentInfo.videoId;
        vaObj.debugLogging = false;
        vaObj.useSSL = window.location.protocol == "https:";
        vaObj.assetDuration = contentInfo.duration;
        vaObj.enableChapterTracking = false;

        /*  Sample code to enable chapter tracking
         vaObj.enableChapterTracking = true;

         let chapters = [];
         let chapterDuration = 60;
         for (let i = 0; i < 3; i++) {
         let chapterData = new AdobePSDK.VA.VideoAnalyticsChapterData("chapter_" + (i+1), i * chapterDuration, chapterDuration, (i+1));
         chapters.push(chapterData);
         }
         vaObj.chapters= chapters;
         */

        vaObj.videoMetadataBlock = function () {
            return {
                "name": contentInfo.title,
                "genre": contentInfo.genre
            };
        };

        vaObj.adMetadataBlock = function () {
            return {
                "name": "my-ad",
                "category": "automotive"
            };
        };

        vaObj.chapterMetadataBlock = function () {
            return {
                "name": "my-chapter",
                "type": "quartile"
            };
        };

        return vaObj;
    }

    getNielsenTrackerExtension() {
        let nielsenTracker = new AdobePSDK.VA.VideoAnalyticsNielsenMetadata();
        nielsenTracker.appInfo = {
            "sfcode": "dcr",
            "clientid": "Adobe",
            "apid": "000000000-0000-0000-0000-000000000000",
            "apn": "Sample Nielsen Player",
            "nol_sdkDebug": "console"
        };
        nielsenTracker.configKey = "0000000000000000000000000000000000000000/000000000000000000000000";

        nielsenTracker.contentMetadataBlock = function () {
            return {
                "type": "content",
                "assetid": "X5432-79567-JJ345-324FT",
                "program": "myProgram-content",
                "category": "Sample category name",
                "title": "The First Episode",
                "length": 60,
                "adloadtype": "2"
            }
        };

        nielsenTracker.adMetadataBlock = function () {
            return {
                "type": "ad",
                "assetid": "assetId-ad"
            }
        };

        nielsenTracker.channelMetadataBlock = function () {
            return {
                "channelName": "Adobe"
            }
        };

        return nielsenTracker;
    }
}

export default VideoAnalytics;
