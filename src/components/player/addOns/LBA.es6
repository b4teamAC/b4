class LBA {

    constructor(playerWrapper) {
        this.player = playerWrapper.getPlayer();
    }

    enableLateBoundAudio() {
        // AA tracks
        this.player.addEventListener(AdobePSDK.PSDKEventType.AUDIO_UPDATED, this.onAudioUpdateEvent);
        this.player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, this.onStatusChange.bind(this));
    }

    /**
     * Alternate Audio Update event handler. Triggered when alternate audio tracks are detected or change.
     * Enabled the alternate audio UI controls.  Note, event is dispatched if the stream contains a
     * single audio track too.
     * @param event
     */
    onAudioUpdateEvent(event) {
        console.warn('%c AA tracks ' + event.item.audioTracks.length, 'background: #ff0000; color: #fff');

        /* Don't enable audio UI controls if stream contains only single audio track */
        if (event.item.audioTracks.length <= 1)
            return;

        let btnSettings = document.getElementById("btn_settings");
        btnSettings.classList.remove("invisible");


        console.warn('%c AA tracks ' + this.options, 'background: #ff0000; color: #fff');
        if (this.selectedIndex) {
            console.log("!!!! TR >", this.options[this.selectedIndex].value);
        }

        for (let i = 0; i < event.item.audioTracks.length; i++) {
            let text = event.item.audioTracks[i].name + ", " + event.item.audioTracks[i].language;

            /*
             if (event.item.audioTracks[i]._default) {
             option.selected = true;
             }
             */
            console.warn('%c redraw all AA options! ' + text, 'background: #ff0000; color: #fff');
        }
    }

    onStatusChange(event) {
        if (event.status === AdobePSDK.MediaPlayerStatus.PREPARED) {
            this.checkAudioTracks();
        }
    }

    checkAudioTracks() {
        let item = this.player.currentItem;
        if (item) {
            let tracks = item.audioTracks;
            console.warn('%c AA tracks available ...' + tracks.length, 'background: #ff0000; color: #fff');
            if (tracks && tracks.length > 0) {
                this.onAudioUpdateEvent.call(this, {
                    item: item
                });
            }
        }
    }

    selectAudioTrack(trackIndex) {
        let index = parseInt(trackIndex);
        if (!Number.isNaN(index)) {
            let item = this.player.currentItem;
            if (item !== AdobePSDK.PSDKErrorCode.ILLEGAL_STATE) {
                let tracks = item.audioTracks;
                if (index >= 0 && index < tracks.length) {
                    item.selectAudioTrack(tracks[index]);
                } else {
                    console.log("ReferencePlayer.selectAudioTrack : Index out of bounds.");
                }
            }
        } else {
            console.log("ReferencePlayer.selectAudioTrack : Invalid argument");
        }
    }
}

export default LBA;
