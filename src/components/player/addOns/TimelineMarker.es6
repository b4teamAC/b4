import ReferencePlayerUtils from "../ReferencePlayerUtils";

class TimelineMarker {

    constructor(playerWrapper) {
        this.player = playerWrapper.getPlayer();
        this.adMarkerHalfSize = 2; // must be half width of adMarker defined in CSS
        this.id = 0;
        this.previousLocalRange = null;
    }

    init() {
        this.cleanup();
        this.player.addEventListener(AdobePSDK.PSDKEventType.TIMELINE_UPDATED, this.onTimelineUpdatedEvent.bind(this));
        this.player.addEventListener(AdobePSDK.PSDKEventType.TIME_CHANGED, this.onTimeChangeEvent.bind(this));
    }

    /**
     * if duration of an AdItem and duration of current player (range.duration)
     * are same then it means that player is for only Ad playing.
     */
    isAdOnlyPlayer(timelineMarkers) {
        let range = this.player.playbackRange;
        if (!(range !== null && typeof range === 'object') || (range.begin === 0 && range.end === 0)) {
            return true;
        }

        let localDuration,
            timeline = this.player.timeline;

        if (timeline) {
            let localBegin = timeline.convertToLocalTime(range.begin),
                localEnd = timeline.convertToLocalTime(range.end);
            localDuration = localEnd - localBegin;
        } else {
            localDuration = range.duration;
        }
        for (let i = 0; i < timelineMarkers.length; i++) {
            let adItems = timelineMarkers[i].items;
            for (let j = 0; j < adItems.length; j++) {
                if (adItems[j].duration === localDuration) {
                    return true;
                }
            }
        }
        return false;
    }

    cleanup() {
        //remove all old markers
        this.id = 0;
        let oldTimelineMarkers = document.getElementsByClassName("adMarker");
        while (oldTimelineMarkers[0]) {
            oldTimelineMarkers[0].parentNode.removeChild(oldTimelineMarkers[0]);
        }
    }

    addTimelineMarker(time, range, backerElm) {
        //time is in local
        this.id++;
        let pos = this.convertTimeToRelativePosition(time, range);
        this.setPosition(pos, backerElm);
    }

    redrawTimelineMarkers(timelineMarkers) {
        this.cleanup();
        if (!this.isAdOnlyPlayer(timelineMarkers)) {
            this.updateTimelineMarkersInUI(timelineMarkers);
        }
    }

    onTimelineUpdatedEvent(event) {
        this.redrawTimelineMarkers(event.timeline.timelineMarkers);
    }

    onTimeChangeEvent(event) {
        //In case of DVR slide, slide the timelineMarkers also accordingly.
        let isLive = ReferencePlayerUtils.isLive(this.player);
        if (isLive) {
            let timeline = this.player.timeline,
                range = this.player.playbackRange,
                localBegin,
                localDuration;
            if (timeline) {
                localBegin = timeline.convertToLocalTime(range.begin);
                let localEnd = timeline.convertToLocalTime(range.end);
                localDuration = localEnd - localBegin;
            } else {
                localBegin = range.begin;
                localDuration = range.duration;
            }

            if (this.previousLocalRange && !(this.previousLocalRange.begin === localBegin && this.previousLocalRange.duration === localDuration)) {
                //handing the case of dvr slide
                //range has been updated. So, update things those needs to be updated on seekbar. For example: timelinemarkers
                let timelineMarkers = timeline.timelineMarkers;
                this.redrawTimelineMarkers(timelineMarkers);
            }
            this.previousLocalRange = new AdobePSDK.TimeRange(localBegin, localDuration);
        }
    }

    updateTimelineMarkersInUI(timelineMarkers) {
        let backerElm = document.getElementById("backer"),
            range = this.player.playbackRange,
            isLive = this.player.currentItem.isLive;
        for (let i = 0; i < timelineMarkers.length; i++) {
            let adPolicy = (isLive || !timelineMarkers[i].isWatched);
            //this adPolicy needs to be in sync with ad policy being implemented.
            if (adPolicy) {
                this.addTimelineMarker(timelineMarkers[i].localRange.begin, range, backerElm);
            }
        }
    }

    convertTimeToRelativePosition(time, range) {
        //time is in local whereas range is in virtual
        let localBegin,
            localDuration,
            localTime;
        // Since time line shows local time, convert them to local times!
        let timeline = this.player.timeline;
        if (timeline) {
            localBegin = timeline.convertToLocalTime(range.begin);
            let localEnd = timeline.convertToLocalTime(range.end);
            localDuration = localEnd - localBegin;
            localTime = time;
        } else {
            localBegin = range.begin;
            localDuration = range.duration;
            localTime = time;
        }
        // convert time to a percentage of the duration
        let position = (localTime - localBegin) / localDuration;
        if (position > 1) position = 1;
        else if (position < 0) position = 0;
        return position;
    }

    setPosition(percent, backerElm) {
        let newItem = document.createElement("div");
        newItem.className = "adMarker round";
        newItem.id = "adMarker_" + this.id;
        newItem.style.left = (percent * 100).toString() + "%";
        backerElm.parentNode.appendChild(newItem);
    }
}

export default TimelineMarker;
