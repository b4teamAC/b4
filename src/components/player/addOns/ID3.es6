let ID3AdTracker = false;

class ID3 {

    constructor(playerWrapper) {
        this.player = playerWrapper.getPlayer();
        this.id3MarkerHalfSize = 2; // must be half width of ID3 defined in CSS
        this._id3Tracker = ID3AdTracker ? new ID3AdTracker() : undefined;
        this.SHOW_MARKER_WINDOW = 5000; // Markers will be visible till SHOW_MARKER_WINDOW ms ahead of current head
        this._timedMetadatas = [];
        this._currentTime = 0;
        this._tdParsing = false; // This variable is used as mutex if in future next call of onTimeChange is made before completion of previous
    }

    init() {
        this.cleanup();
        this.player.addEventListener(AdobePSDK.PSDKEventType.TIMED_METADATA_AVAILABLE, this.onTimedMetadataEvent);
        this.player.addEventListener(AdobePSDK.PSDKEventType.TIME_CHANGED, this.onTimeChange);
    }

    cleanup() {
        //remove all old markers
        let old = document.getElementsByClassName("id3Marker");
        for (let i = 0; i < old.length;) {
            old[i].parentNode.removeChild(old[i]);
            old = document.getElementsByClassName("id3Marker");
        }
        this._timedMetadatas = [];
    }

    static compareTimedMetadata(a, b) {
        return a.td.time - b.td.time;
    }

    updateTimedMetadata(event) {
        let tdContainer = {
            td: event.timedMetadata,
            adTracking: false,
            visibleInUI: false,
            beginTime: 0
        };
        this._timedMetadatas.push(tdContainer);
        this._timedMetadatas.sort(ID3.compareTimedMetadata);
    }

    deleteOldTimedMetadata() {
        let itemsDeleted = [];
        if (this._timedMetadatas) {
            let playbackRange = this.player.playbackRange,
                begin = playbackRange.begin;

            for (let i = 0; i < this._timedMetadatas.length;) {
                let time = this._timedMetadatas[i].td.time;
                if (time >= begin) {
                    return itemsDeleted;
                }
                itemsDeleted.push(this._timedMetadatas.shift());
            }
        }
        return itemsDeleted;
    }

    onTimedMetadataEvent(event) {
        //cleanup();
        //var timedMetadataList = getPlayer().currentItem.timedMetadata;
        this.updateTimedMetadata(event);

        this.updateMarkersInUI();
    }

    updateMarkersInUI() {
        let timedMetadataList = this._timedMetadatas;
        if (!timedMetadataList || timedMetadataList.length === 0)
            return;

        let itemsDeleted = this.deleteOldTimedMetadata();
        for (let i = 0; i < itemsDeleted.length; i++) {
            ID3.removeID3Marker(itemsDeleted[i]);
        }

        let backerElm = document.getElementById("backer");
        for (let i = 0; i < timedMetadataList.length; i++) {
            let td = timedMetadataList[i].td;
            if (td.type === AdobePSDK.TimedMetadataType.TAG || td.type === "TAG")
                continue;
            let time = td.time;
            if (this._currentTime < (time - this.SHOW_MARKER_WINDOW)) {
                break;
            }
            //let range = event.target.getPlaybackRange();
            let range = this.player.playbackRange;
            this.addID3Marker(time, range, backerElm, timedMetadataList[i]);
        }
    }

    onTimeChange(event) {
        if (this._tdParsing === true) {
            return;
        }

        this._tdParsing = true;
        this._currentTime = event.time;
        let timedMetadataList = this._timedMetadatas;
        if (!timedMetadataList || timedMetadataList.length === 0) {
            this._tdParsing = false;
            return;
        }

        if (this._id3Tracker) {
            this._id3Tracker.id3Detected(this._currentTime, timedMetadataList);
        }

        this.updateMarkersInUI();
        this._tdParsing = false;
    }

    static removeID3Marker(tdContainer) {
        let marker = document.getElementById("id3_" + tdContainer.td.id);
        if (marker) {
            marker.parentNode.removeChild(marker);
            tdContainer.visibleInUI = false;
        }
    }

    addID3Marker(time, range, backerElm, tdContainer) {
        if (tdContainer.visibleInUI === true) {
            if (tdContainer.beginTime === range.begin) {
                return;
            } else {
                // Removing the marker so that it is added as per new DVR window
                ID3.removeID3Marker(tdContainer);
            }
        }

        tdContainer.beginTime = range.begin;
        tdContainer.visibleInUI = true;
        let pos = this.convertTimeToRelativePosition(time, range);
        this.setPosition(pos, backerElm, tdContainer.td);
    }

    convertTimeToRelativePosition(time, range) {
        let localBegin,
            localDuration,
            localTime;

        // Since time line shows local time, convert them to local times!
        let timeline = this.player.timeline;
        if (timeline) {
            localBegin = timeline.convertToLocalTime(range.begin);
            let localEnd = timeline.convertToLocalTime(range.end);
            localDuration = localEnd - localBegin;
            localTime = timeline.convertToLocalTime(time);
        } else {
            localBegin = range.begin;
            localDuration = range.duration;
            localTime = time;
        }
        // convert time to a percentage of the duration
        let position = (localTime - localBegin) / localDuration;
        if (position > 1) position = 1;
        else if (position < 0) position = 0;
        return position;
    }

    hex2a(hex, offset, max) {
        let str = '';
        if (!hex)
            return str;
        for (let i = offset; i < hex.length && i < offset + max; i++)
            str += String.fromCharCode(hex[i]);
        return str;
    }

    setPosition(percent, backerElm, td) {
        // adjust to keep id3 marker inside container on right side
        let bounds = backerElm.getBoundingClientRect(),
            pos = percent * (bounds.right - bounds.left);

        let newItem = document.createElement("div");
        newItem.className = "id3Marker round";
        newItem.id = "id3_" + td.id;
        newItem.style.left = (pos - this.id3MarkerHalfSize).toString() + "px";
        backerElm.parentNode.appendChild(newItem);

        let md = td.metadata,
            keySet = md.keySet;
        if (keySet && keySet.length) {
            let msg = '';
            for (let j = 0; j < keySet.length; j++) {
                let idTag = keySet[j];
                msg += idTag;
                if (idTag.indexOf("T") === 0) {
                    /* text frame : encoding byte + text data
                     * 00=ASCII;01=UTF-16 Unicode with BOM;02=UTF-16BE Unicode without BOM
                     * 03=UTF-8
                     */
                    let buff = md.getByteArray(idTag);
                    msg += " : " + this.hex2a(buff, 1, buff.length - 1);
                }
                msg += " ; ";
            }
            newItem.title = msg;
        }
    }
}

export default ID3;
