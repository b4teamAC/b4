class ClickableAds {

    constructor(playerWrapper) {
        this.playerWrapper = playerWrapper;
        this.clickAddButton = document.getElementById("adClickButton");
    }

    onAdStarted(event) {
        if (event && event.ad) {
            let adClick = event.ad.primaryAsset && event.ad.primaryAsset.adClick;
            if (adClick && adClick.isValid) {
                this.clickAddButton.removeAttribute('hidden');
            }
        }
    }

    onAdCompleted(event) {
        if (this.clickAddButton) {
            this.clickAddButton.setAttribute('hidden', 'hidden');
        }
    }

    onAdClickedEvent(event) {
        if (event && event.ad) {
            let adClick = event.adClick;
            if (!(adClick && adClick.isValid)) {
                adClick = event.ad.primaryAsset && event.ad.primaryAsset.adClick;
            }
            if (adClick && adClick.isValid) {
                let player = this.playerWrapper.getPlayer();
                if (player) {
                    player.pause();
                }
                window.open(adClick.url);
            }
        }
    }

    onAdClick(event) {
        this.playerWrapper.getPlayer().notifyClick();
    }

    enableAdEvents() {
        this.playerWrapper.getPlayer().addEventListener(AdobePSDK.PSDKEventType.AD_STARTED, this.onAdStarted);
        this.playerWrapper.getPlayer().addEventListener(AdobePSDK.PSDKEventType.AD_COMPLETED, this.onAdCompleted);
        this.playerWrapper.getPlayer().addEventListener(AdobePSDK.PSDKEventType.AD_CLICK, this.onAdClickedEvent);
        this.clickAddButton.addEventListener("click", this.onAdClick);
    }
}

export default ClickableAds;
