class DRM {

    constructor(playerWrapper) {
        this.drmLicense = null;
        this.useLicenseCallback = false;
        this.drmUsername = "";
        this.drmPassword = "";

        this.fpsWithLicenseCallback = {
            "com.apple.fps.1_0": {
                "licenseResponseType": "text",
                "httpRequestHeaders": {
                    "Content-type": "application/x-www-form-urlencoded"
                }
            }
        };

        this.adobeFPSData = {
            "com.apple.fps.1_0": {
                "serverURL": "http://fairplay-fps.corp.adobe.com:8080/cgi-bin/fps.cgi?",
                "licenseResponseType": "arraybuffer",
                "httpRequestHeaders": {
                    "Content-type": "application/octet-stream"
                }
            }
        };

        this.player = playerWrapper.getPlayer();
    }

    enableDRMEvents() {
        let protectionData = {
            "com.adobe.primetime": {
                "serverURL": {
                    "individualization-request": "http://individualization.adobe.com/flashaccess/i15n/v5",
                    "license-request": "http://chives.corp.adobe.com:8096/flashaccess/req",
                    "license-release": "http://chives.corp.adobe.com:8096/flashaccess/req"
                },
                "httpRequestHeaders": {}
            },
            "com.widevine.alpha": {
                "serverURL": "https://wv.service.expressplay.com/hms/wv/rights/?ExpressPlayToken=AQAAABIDKA4AAABQuPPoebWWZZD2l3APRKkkagEDOXmCjgbhsqJTYeZ9KabkjCvSLvuXGHiVLymBnouGXDdCKpbz5IvB3jCZp9U05pysl9eavucsWXnA0tafbM-1SSJKXOa70kvxAJ_ybhdcmy7-6g"
            },
            "com.microsoft.playready": {
                "serverURL": "https://expressplay-licensing.axprod.net/LicensingService.ashx?ExpressPlayToken=AQAAAw_ZXqcAAABgHD1gnn_AMQJKfFCP3k9zbBw2srzBLryJVLXclnjhcSBCz4TBzrtfegmSw1hAKdFHTNL-KVBGsI4ygBnfPRBUCvGsVOwpQ944fhq45W06ygJroB2xOrM03tbkWcrthI7y_UQdHzufHjcBqKZm8QDoqKpxrxc"
            },
            /*
             "com.apple.fps.1_0": fpsWithLicenseCallback["com.apple.fps.1_0"],
             */
            //For Adobe
            "com.apple.fps.1_0": this.adobeFPSData["com.apple.fps.1_0"],

            "org.w3.clearkey": {
                "clearkeys": {
                    "H3JbV93QV3mPNBKQON2UtQ": "ClKhDPHMtCouEx1vLGsJsA",
                    "IAAAACAAIAAgACAAAAAAAg": "5t1CjnbMFURBou087OSj2w"
                }
            }
        };
        this.player.addEventListener(AdobePSDK.PSDKEventType.DRM_METADATA_INFO_AVAILABLE, this.onDRMMetadataInfoAvailable);
    }

    static base64EncodeUint8Array(input) {
        let keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            output = "",
            chr1, chr2, chr3, enc1, enc2, enc3, enc4,
            i = 0;

        while (i < input.length) {
            chr1 = input[i++];
            chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index
            chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (Number.isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (Number.isNaN(chr3)) {
                enc4 = 64;
            }
            output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                keyStr.charAt(enc3) + keyStr.charAt(enc4);
        }
        return output;
    }

    onLicenseMessageCallback(drmLicenseRequest) {
        let licenseMessage = drmLicenseRequest.licenseMessage,
            assetId = drmLicenseRequest.assetId,
            base64Message = DRM.base64EncodeUint8Array(licenseMessage),
            messageStruct = {
                "assetId": assetId,
                "spc": base64Message
            };

        let messageRequest = JSON.stringify(messageStruct);
        return messageRequest;
    }

    onDRMOperationCompleteFunc() {
        console.log("---------DRM SET AUTHENTICATION TOKEN-------- success ");
    }

    onLicenseReturnFunc() {
        console.log("DRM: License Return Complete ");
    }

    onLicenseReturnErrorFunc(major, minor, errorString /*, errorServerUrl*/) {
        console.log("DRM: License Return Error: " + errorString);
    }

    returnLicense() {
        if (this.drmLicense) {
            let drmManager = this.player.drmManager;
            if (drmManager) {
                let returnLicenseListener = new AdobePSDK.DRMReturnLicenseListener(this.onLicenseReturnFunc, this.onLicenseReturnErrorFunc);
                drmManager.returnLicense(null, null, null, false, returnLicenseListener, this.drmLicense.session);
            }
        }
    }

    onAuthenticationCompleteFunc(authenticationToken) {
        let token = authenticationToken,
            currentItem = this.player.currentItem,
            arrDRMMetadataInfosObj = currentItem.drmMetadataInfos,
            drmMetadata = null,
            arrDRMPolicy = null,
            licenseDomain = null,
            drmManager = null;

        if (arrDRMMetadataInfosObj && arrDRMMetadataInfosObj.length > 0) {
            let drmMetadataInfos = arrDRMMetadataInfosObj[0];

            if (drmMetadataInfos) {
                drmMetadata = drmMetadataInfos.drmMetadata;
            }

            if (drmMetadata) {
                arrDRMPolicy = drmMetadata.policies;
                if (arrDRMPolicy && arrDRMPolicy.length > 0) {
                    let drmPolicy = arrDRMPolicy[0];
                    if (drmPolicy) {
                        licenseDomain = drmPolicy.licenseDomain;
                        drmManager = this.player.drmManager;

                        let setAuthenticationTokenOperationCompleteListener = new AdobePSDK.DRMOperationCompleteListener(this.onDRMOperationCompleteFunc, this.onAcquireLicenseErrorFunc);
                        drmManager.setAuthenticationToken(drmMetadata, drmPolicy.authenticationDomain, token, setAuthenticationTokenOperationCompleteListener);
                    }
                }
            }
        }

        console.log("DRM: License Acquired : " + DRMLicense.session + " " + DRMLicense.message);
    }

    onAcquireLicenseFunc(DRMLicense) {
        this.drmLicense = DRMLicense;
        console.log("--------DRM License Acquired------- Success");
    }

    onAcquireLicenseErrorFunc(major, minor, errorString /*, errorServerUrl*/) {
        console.log("DRM: License Acquire Error: " + errorString);
    }

    onDRMMetadataInfoAvailable(event) {
        console.log(event.type);

        let drmMetadataInfo = event.drmMetadataInfo,
            drmMetadata = null,
            arrDRMPolicy = null,
            drmLicenseDomain = null;

        if (drmMetadataInfo) {
            drmMetadata = drmMetadataInfo.drmMetadata;
        }

        if (drmMetadata) {
            arrDRMPolicy = drmMetadata.policies;
            drmLicenseDomain = drmMetadata.licenseId;
            console.log("#DRMMetadataInfo  serverUrl:" + drmMetadata.serverUrl + " Policies:" + drmMetadata.policies + " LicenseId:" + drmMetadata.licenseId);
        }

        let drmManager = this.player.drmManager,
            authenticateListener = new AdobePSDK.DRMAuthenticateListener(this.onAuthenticationCompleteFunc, this.onAcquireLicenseErrorFunc),
            acquireLicenseListener = new AdobePSDK.DRMAcquireLicenseListener(this.onAcquireLicenseFunc, this.onAcquireLicenseErrorFunc);

        if (arrDRMPolicy && arrDRMPolicy.length > 0) {
            let drmPolicy = arrDRMPolicy[0];
            if (drmPolicy.authenticationMethod == AdobePSDK.DRMAuthenticationMethod.USERNAME_AND_PASSWORD) {
                drmManager.authenticate(drmMetadata, drmMetadata.serverUrl, drmPolicy.authenticationDomain, this.drmUsername, this.drmPassword, authenticateListener);
            }
            /*else {
             if(drmLicenseDomain.authenticationMethod == AdobePSDK.DRMAuthenticationMethod.USERNAME_AND_PASSWORD) {
             setDRMAuthenticate();
             drmManager.authenticate(drmMetadata, drmLicenseDomain.serverUrl, drmLicenseDomain.authenticationDomain, "testuser", "testpass", authenticateListener);
             }
             }*/
        }

        if (drmManager) {
            if (this.useLicenseCallback) {
                drmManager.acquireLicense(drmMetadata, null, acquireLicenseListener, this.onLicenseMessageCallback);
            } else {
                drmManager.acquireLicense(drmMetadata, null, acquireLicenseListener);
            }
        }
    }

    setDRMSettings(DRMSettings) {
        let protectionData = DRMSettings;
        if (protectionData) {
            let drmMgr = this.player.drmManager;
            if (drmMgr) {
                drmMgr.setProtectionData(protectionData);
            }
        }
    }
}

export default DRM;
