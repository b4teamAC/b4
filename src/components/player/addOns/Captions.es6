import {TextFormatBuilder} from "./SettingsManager";

class Captions {
    constructor(playerWrapper) {
        this.playerWrapper = playerWrapper;
        this.btnClosedCaptions = document.getElementById("btn_captions");
    }

    enableCaptions(isPIPPlayer) {
        // Captions
        this.btnClosedCaptions.addEventListener("click", function () {
            if (isPIPPlayer) {
                return;
            }
            let result = this.toggleCCVisibility();
            this.toggleCCButtonStyle(result.visible);
        }.bind(this));

        let player = this.playerWrapper.getPlayer();
        player.addEventListener(AdobePSDK.PSDKEventType.CAPTIONS_UPDATED, this.onCaptionsUpdateEvent.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.STATUS_CHANGED, this.onStatusChange.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AD_BREAK_STARTED, this.onAdBreakStarted.bind(this));
        player.addEventListener(AdobePSDK.PSDKEventType.AD_BREAK_COMPLETED, this.onAdBreakCompleted.bind(this));
    }

    /**
     * Closed Captions Update event handler. Triggered when closed captions tracks are detected or change.
     * Enables the closed captions UI controls.
     * @param event
     */
    onCaptionsUpdateEvent(event) {
        // show closed captions toggle button
        let btnCC = document.getElementById("btn_captions");
        btnCC.classList.remove("invisible");

        let btnSettings = document.getElementById("btn_settings");
        btnSettings.classList.remove("invisible");

        console.log("!!!! CC track >", this.selectedIndex, this.options);
        if (this.selectedIndex) {
            console.log("!!!! TR >", this.options[this.selectedIndex].value);
        }

        // decorate button if captions are enabled/disabled
        console.warn('%c get CC visibility .. styling button> ' + this.getCCVisibility(), 'background: #ffaa00; color: #fff');
        //this.toggleCCButtonStyle(this.getCCVisibility());

        // remove old options
        let eventData = event.item.closedCaptionsTracks;
        console.warn('%c CC tracks ' + eventData.length, 'background: #ffaa00; color: #fff');
        for (let i = 0; i < eventData.length; i++) {
            let text = eventData[i].name + " (" + eventData[i].language + ") [" + eventData[i].serviceType + "]";
            /*
             NOTE
             We don't enable Closed Captions by default, hence the following code has no effect.
             But it does disable CC when there is only one Caption to select.
             if (eventData[i].isDefault)
             {
             option.selected = true;
             }
             */
            console.warn('%c redraw all CC options! ' + text, 'background: #ffaa00; color: #fff');
        }
    }

    /*
     * Player StatusChange event listener. Installed here to detect updated  closed captions tracks
     * for cases where CaptionsUpdate Event is not being received - VTT in Flash Player until the
     * issue is resolved.
     */
    onStatusChange(event) {
        if (event.status === AdobePSDK.MediaPlayerStatus.PREPARED) {
            let builder = new TextFormatBuilder(this.playerWrapper.getPlayer().ccStyle);
            builder.backgroundOpacity = "100";
            this.playerWrapper.getPlayer().ccStyle = builder.build();

            this.checkClosedCaptions();
        }
    }

    checkClosedCaptions() {
        let item = this.playerWrapper.getPlayer().currentItem;
        if (item) {
            let tracks = item.closedCaptionsTracks;
            if (tracks && tracks.length > 0) {
                this.onCaptionsUpdateEvent({
                    item: item
                });
            }
        }

    }

    /**
     * Toggles UI style for the closed captions when the captions are turned on/off.
     * @param visible {boolean}
     */
    toggleCCButtonStyle(visible) {
        if (visible) {
            document.getElementById("btn_captions").classList.remove("captions-off");
        } else {
            document.getElementById("btn_captions").classList.add("captions-off");
        }
    }

    toggleCCVisibility() {
        if (this.getCCVisibility() === false) {
            this.playerWrapper.getPlayer().ccVisibility = AdobePSDK.MediaPlayer.VISIBLE;
            return {
                visible: true
            };
        } else {
            this.playerWrapper.getPlayer().ccVisibility = AdobePSDK.MediaPlayer.INVISIBLE;
            return {
                visible: false
            };
        }
    }

    getCCVisibility() {
        let visibility = this.playerWrapper.getPlayer().ccVisibility;
        return visibility === AdobePSDK.MediaPlayer.VISIBLE;
    }

    getCCStyle() {
        return this.playerWrapper.getPlayer().ccStyle;
    }

    selectCaptionsTrack(trackIndex) {
        let index = parseInt(trackIndex);
        if (!Number.isNaN(index)) {
            let item = this.playerWrapper.getPlayer().currentItem;
            if (item !== AdobePSDK.PSDKErrorCode.ILLEGAL_STATE) {
                let tracks = item.closedCaptionsTracks;
                if (index >= 0 && index < tracks.length) {
                    item.selectClosedCaptionsTrack(tracks[index]);
                } else {
                    console.log("ReferencePlayer.selectCaptionsTrack : Index out of bounds.");
                }
            }
        } else {
            console.log("ReferencePlayer.selectCaptionsTrack : Invalid argument");
        }
    }

    onAdBreakStarted(event) {
        this.disableCCControls();
    }

    onAdBreakCompleted(event) {
        this.enableCCControls();
    }

    disableCCControls() {
        let btnCC = document.getElementById("btn_captions");
        btnCC.classList.add("captions-off");
        btnCC.disabled = true;

        let btnSettings = document.getElementById("btn_settings");
        btnSettings.classList.remove("on");
        btnSettings.disabled = true;

        let settingsContainer = document.getElementById("settings-container");
        settingsContainer.setAttribute("hidden", "true");
    }

    enableCCControls() {
        let btnCC = document.getElementById("btn_captions");
        btnCC.disabled = false;

        let btnSettings = document.getElementById("btn_settings");
        btnSettings.disabled = false;
    }
}

export default Captions;
