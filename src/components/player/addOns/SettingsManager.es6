/**
 * Helper objects
 * ReferencePlayer.TextFormat.Color
 * ReferencePlayer.TextFormat.Size
 * ReferencePlayer.TextFormat.Font
 * ReferencePlayer.TextFormat.FontEdge
 * ReferencePlayer.TextFormatBuilder
 * ReferencePlayer.SettingsManager
 * ReferencePlayer.SettingsManager.ClosedCaptionsSettings
 *
 */

export let Color = {
    COLOR_DEFAULT: AdobePSDK.TextFormat.COLOR_DEFAULT,
    COLOR_BLACK: AdobePSDK.TextFormat.BLACK,
    COLOR_GRAY: AdobePSDK.TextFormat.GRAY,
    COLOR_WHITE: AdobePSDK.TextFormat.WHITE,
    COLOR_BRIGHT_WHITE: AdobePSDK.TextFormat.BRIGHT_WHITE,
    COLOR_DARK_RED: AdobePSDK.TextFormat.DARK_RED,
    COLOR_RED: AdobePSDK.TextFormat.RED,
    COLOR_BRIGHT_RED: AdobePSDK.TextFormat.BRIGHT_RED,
    COLOR_DARK_GREEN: AdobePSDK.TextFormat.DARK_GREEN,
    COLOR_GREEN: AdobePSDK.TextFormat.GREEN,
    COLOR_BRIGHT_GREEN: AdobePSDK.TextFormat.BRIGHT_GREEN,
    COLOR_DARK_BLUE: AdobePSDK.TextFormat.DARK_BLUE,
    COLOR_BLUE: AdobePSDK.TextFormat.BLUE,
    COLOR_BRIGHT_BLUE: AdobePSDK.TextFormat.BRIGHT_BLUE,
    COLOR_DARK_YELLOW: AdobePSDK.TextFormat.DARK_YELLOW,
    COLOR_YELLOW: AdobePSDK.TextFormat.YELLOW,
    COLOR_BRIGHT_YELLOW: AdobePSDK.TextFormat.BRIGHT_YELLOW,
    COLOR_DARK_MAGENTA: AdobePSDK.TextFormat.DARK_MAGENTA,
    COLOR_MAGENTA: AdobePSDK.TextFormat.MAGENTA,
    COLOR_BRIGHT_MAGENTA: AdobePSDK.TextFormat.BRIGHT_MAGENTA,
    COLOR_DARK_CYAN: AdobePSDK.TextFormat.DARK_CYAN,
    COLOR_CYAN: AdobePSDK.TextFormat.CYAN,
    COLOR_BRIGHT_CYAN: AdobePSDK.TextFormat.BRIGHT_CYAN
};

export let Size = {
    SIZE_DEFAULT: AdobePSDK.TextFormat.SIZE_DEFAULT,
    SIZE_SMALL: AdobePSDK.TextFormat.SMALL,
    SIZE_MEDIUM: AdobePSDK.TextFormat.MEDIUM,
    SIZE_LARGE: AdobePSDK.TextFormat.LARGE
};

export let Font = {
    FONT_DEFAULT: AdobePSDK.TextFormat.FONT_DEFAULT,
    FONT_MONOSPACED_WITH_SERIFS: AdobePSDK.TextFormat.MONOSPACED_WITH_SERIFS,
    FONT_PROPORTIONAL_WITH_SERIFS: AdobePSDK.TextFormat.PROPORTIONAL_WITH_SERIFS,
    FONT_MONOSPACED_WITHOUT_SERIFS: AdobePSDK.TextFormat.MONOSPACED_WITHOUT_SERIFS,
    FONT_CASUAL: AdobePSDK.TextFormat.CASUAL,
    FONT_CURSIVE: AdobePSDK.TextFormat.CURSIVE,
    FONT_SMALL_CAPITALS: AdobePSDK.TextFormat.SMALL_CAPITALS
};

export let FontEdge = {
    FONT_EDGE_DEFAULT: AdobePSDK.TextFormat.FONT_EDGE_DEFAULT,
    FONT_EDGE_NONE: AdobePSDK.TextFormat.NONE,
    FONT_EDGE_RAISED: AdobePSDK.TextFormat.RAISED,
    FONT_EDGE_DEPRESSED: AdobePSDK.TextFormat.DEPRESSED,
    FONT_EDGE_UNIFORM: AdobePSDK.TextFormat.UNIFORM,
    FONT_EDGE_DROP_SHADOW_LEFT: AdobePSDK.TextFormat.DROP_SHADOW_LEFT,
    FONT_EDGE_DROP_SHADOW_RIGHT: AdobePSDK.TextFormat.DROP_SHADOW_RIGHT
};

export let TextFormatBuilder = function (textFormat) {

    let _font = textFormat.font,
        _size = textFormat.size,
        _fontColor = textFormat.fontColor,
        _fontEdge = textFormat.fontEdge,
        _edgeColor = textFormat.edgeColor,
        _fontOpacity = textFormat.fontOpacity,
        _backgroundColor = textFormat.backgroundColor,
        _backgroundOpacity = textFormat.backgroundOpacity,
        _fillColor = textFormat.fillColor,
        _fillOpacity = textFormat.fillOpacity,
        _fontLength = Object.keys(Font).length,
        _colorLength = Object.keys(Color).length,
        _sizeLength = Object.keys(Size).length,
        _edgeLength = Object.keys(FontEdge).length;


    this.build = function () {
        return new AdobePSDK.TextFormat(
            _font,
            _fontColor,
            _edgeColor,
            _fontEdge,
            _backgroundColor,
            _fillColor,
            _size,
            _fontOpacity,
            _backgroundOpacity,
            _fillOpacity
        );
    };

    Object.defineProperty(this, "font", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value <= _fontLength) {
                _font = value;
            }
        },
        get: function () {
            return _font;
        }
    });


    Object.defineProperty(this, "size", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _sizeLength) {
                _size = value;
            }
        },
        get: function () {
            return _size;
        }
    });


    Object.defineProperty(this, "fontColor", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _colorLength) {
                _fontColor = value;
            }
        },
        get: function () {
            return _fontColor;
        }
    });

    Object.defineProperty(this, "fontEdge", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _edgeLength) {
                _fontEdge = value;
            }
        },
        get: function () {
            return _fontEdge;
        }
    });


    Object.defineProperty(this, "edgeColor", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _colorLength) {
                _edgeColor = value;
            }
        },
        get: function () {
            return _edgeColor;
        }
    });


    Object.defineProperty(this, "fontOpacity", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value >= 0 && value <= 100) {
                _fontOpacity = value;
            }
        },
        get: function () {
            return _fontOpacity;
        }
    });

    Object.defineProperty(this, "backgroundColor", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _colorLength) {
                _backgroundColor = value;
            }
        },
        get: function () {
            return _backgroundColor;
        }
    });

    Object.defineProperty(this, "backgroundOpacity", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value >= 0 && value <= 100) {
                _backgroundOpacity = value;
            }
        },
        get: function () {
            return _backgroundOpacity;
        }
    });

    Object.defineProperty(this, "fillColor", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value > -1 && value < _colorLength) {
                _fillColor = value;
            }
        },
        get: function () {
            return _fillColor;
        }
    });

    Object.defineProperty(this, "fillOpacity", {
        set: function (value) {
            if (typeof value !== 'undefined' &&
                value >= 0 && value <= 100) {
                _fillOpacity = value;
            }
        },
        get: function () {
            return _fillOpacity;
        }
    });

};

export class Setting {

    constructor() {
        /**
         * ID used to reference this setting.
         */
        this.id = undefined;

        /**
         * Named used in the main settings panel which links to this setting.
         */
        this.name = undefined;

        /**
         * The top-level DOM element which displays the settings.
         */
        this.settingsDOM = undefined;
    }

    /**
     * Function which hides the settingsDOM element.
     */
    hide() {
        //TODO [brom] fix it
        //this.settingsDOM.setAttribute("hidden", "true");
    }

    /**
     * Function which displays the settingsDOM element.
     */
    display() {
        //TODO [brom] fix it
        //this.settingsDOM.removeAttribute("hidden");
    }
}

/**
 * Settings Manager used to register settings panels and control their display.
 */
class SettingsManager {

    constructor() {
        this.settings = {};
        // create settings manager container
        this.container = document.getElementById("settings-container");
        /*
         container = document.createElement("DIV");
         container.id = "settings-container";
         container.setAttribute("hidden", "hidden");
         document.querySelector("#popup-panel").appendChild(container);
         */

        // initialize settings manager when constructed
        this.init();
    }

    init() {
        // add the main settings panel so it can be hidden/displayed using settings manager
        let main = new Setting();
        main.id = 'settings-panel-main';
        main.name = 'Settings';

        this.settings[main.id] = main;
    }

    /**
     * Register a new Setting object to the manager. Adds a button to the main
     * settings panel which when selected will display the settings set in settingsDOM.
     * @param setting ReferencePlayer.SettingsManager.Setting object
     */
    register(setting) {
        console.log("REGISTERING SETTING >", setting);
    }

    /**
     * Reset this settings manager by removing all settings panels from the DOM.
     */
    reset() {
        this.hideManager();

        // remove all elements from the DOM
        while (this.container.firstChild) {
            this.container.removeChild(this.container.firstChild);
        }

        // clear settings array
        this.settings = {};

        // re-initialize manager
        this.init();
    }

    /**
     * Hides all settings panels. Does not hide the settings manager container.
     */
    hideAll() {
        let keys = Object.keys(this.settings);
        for (let i = 0; i < keys.length; i++) {
            this.settings[keys[i]].hide();
        }
    }

    /**
     * Hides the settings manager container DOM element and all settings panels.
     * Hide settings options. Call this from a "settings" button.
     */
    hideManager() {
        this.container.setAttribute("hidden", "hidden");
        this.hideAll();
    }

    /**
     * Displays the settings manager container DOM element and the main settings panel.
     * Display settings options. Call this from a "settings" button.
     */
    displayManager() {
        this.container.removeAttribute("hidden");
    }

    /**
     * Toggles display of the settings options.
     * @returns {{visible: boolean}} true if the settings are now displayed,
     * false if the settings are now hidden.
     */
    toggleDisplay() {
        if (this.container.hasAttribute("hidden")) {
            this.displayManager();
            return {
                visible: true
            };
        } else {
            this.hideManager();
            return {
                visible: false
            };
        }
    }

    /**
     * Display a specific settings panel.
     * @param id - settings panel identifier.
     */
    displaySettingsFor(id) {
        if (this.containsSetting(id)) {
            this.hideAll();
            this.settings[id].display();
        }
    }

    /**
     * Check if a given settings is already registered with this manager.
     * @param id the Setting.id
     */
    containsSetting(id) {
        return this.settings.hasOwnProperty(id);
    }
}

export default SettingsManager;
