import ReferencePlayerEvents from "../ReferencePlayerEvents";
import ReferencePlayerUtils from "../ReferencePlayerUtils";

class Ads {
    constructor(playerWrapper) {
        this.playerWrapper = playerWrapper;
        this.videoDiv = playerWrapper.getVideoDiv();
        this.adRow = this.videoDiv.querySelector("#ad-row");
        this.adRemainingTime = this.videoDiv.querySelector("#ad-remaining-time");
        this.adProgressElm = this.videoDiv.querySelector("#ad-progress");
        this.adBackerElm = this.videoDiv.querySelector("#ad-backer");
    }

    enableAdEvents() {
        this.playerWrapper.addEventListener(AdobePSDK.PSDKEventType.AD_BREAK_STARTED, this.onAdbreakStarted);
        this.playerWrapper.addEventListener(AdobePSDK.PSDKEventType.AD_BREAK_COMPLETED, this.onAdbreakCompleted);
        this.playerWrapper.addEventListener(AdobePSDK.PSDKEventType.AD_STARTED, this.onAdStarted);
        this.playerWrapper.addEventListener(AdobePSDK.PSDKEventType.AD_PROGRESS, this.onAdProgress);
        this.playerWrapper.addEventListener(AdobePSDK.PSDKEventType.AD_COMPLETED, this.onAdCompleted);
    }

    onAdbreakStarted() {
        this.playerWrapper.setControlsDisabledInAd(true);
        this.playerWrapper.dispatchEventWrapper(ReferencePlayerEvents.LogEvent, {
            message: "onAdBreakStartedEvent"
        });
    }

    onAdbreakCompleted() {
        this.playerWrapper.setControlsDisabledInAd(false);
        this.playerWrapper.dispatchEventWrapper(ReferencePlayerEvents.LogEvent, {
            message: "onAdBreakCompletedEvent"
        });
    }

    onAdStarted() {
        if (this.adRemainingTime && this.adProgressElm) {
            this.adRemainingTime.innerHTML = "Ad";
            this.adProgressElm.style.width = "0px";
        }
        this.playerWrapper.setControlsDisabledInAd(true);
        this.playerWrapper.dispatchEventWrapper(ReferencePlayerEvents.LogEvent, {
            message: "onAdStartedEvent"
        });
    }

    onAdProgress(event) {
        if (this.adRow && this.adRemainingTime && this.adBackerElm && this.adProgressElm) {
            if (this.adRow.hasAttribute("hidden")) {
                this.adRow.removeAttribute("hidden");
            }

            let duration = 1000 * Math.round(event.ad.duration / 1000),
                time = 1000 * Math.round(event.time / 1000),
                formatted_time = ReferencePlayerUtils.formatDisplayTime(duration - time, duration);
            this.adRemainingTime.innerHTML = "Ad &nbsp;" + formatted_time;

            let bounds = adBackerElm.getBoundingClientRect(),
                pos = (event.progress / 100) * (bounds.right - bounds.left);
            this.adProgressElm.style.width = (pos).toString() + "px";
        }
    }

    onAdCompleted() {
        if (this.adRow) {
            this.adRow.setAttribute("hidden", true);
        }
        this.playerWrapper.setControlsDisabledInAd(false);
        this.playerWrapper.dispatchEventWrapper(ReferencePlayerEvents.LogEvent, {
            message: "onAdCompletedEvent"
        });
    }
}

export default Ads;
