let ReferencePlayerEventStatus = {
    /**
     * Dispatched to notify that load has been pressed
     */
    PLAYER_STATUS_COMPLETE: AdobePSDK.MediaPlayerStatus.COMPLETE
};

export default ReferencePlayerEventStatus;
