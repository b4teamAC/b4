let ReferencePlayerEvents = {
    /**
     * Dispatched to notify that load has been pressed
     */
    LoadInitiatedEvent: "onLoadInitiated",
    /**
     * Dispatched to notify that load has been pressed
     */
    StatusChangeEvent: "onStatusChange",
    /**
     * Logging event
     */
    LogEvent: "onLogEvent",
    /**
     * Throttling event
     */
    ThrottleEvent: "onThrottleEvent"
};

export default ReferencePlayerEvents;
