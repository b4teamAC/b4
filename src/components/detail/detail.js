import React, {Component} from 'react';
import {connect} from 'react-redux';
import cssmodules from 'react-css-modules';
import styles from './detail.cssmodule.scss';
import Menu from '../menu/menu';
import DetailHeader from './detail-header';
import SubHeader from './sub-header';
import DetailMain from './detail-main';

export class Detail extends Component {

    render() {
        return (
          <div>
            <Menu />
            <div styleName="detail-container">
              <DetailHeader />
              <SubHeader />
              <DetailMain />
            </div>
          </div>
        );
    }
}


function mapStateToProps(state) {
    return {};
}

Detail.displayName = 'detailsPage';
Detail.propTypes = {};
Detail.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(Detail, styles, {allowMultiple: true}));
