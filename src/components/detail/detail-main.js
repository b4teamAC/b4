import React, {Component} from "react";
import {connect} from "react-redux";
import Player from "../player/player";
import cssmodules from "react-css-modules";
import styles from "./detail.cssmodule.scss";
import * as detailActions from "../../actions/detail-actions";
import apis from "../../static/constants";

export class DetailMain extends Component {

    componentWillMount() {
        this.props.dispatch(detailActions.getDetailMainData(apis.getDetailMainData));
    }

    getDetailPage() {
        const contentInfo = this.props.contentInfo;
        if (contentInfo) {
            return (
              <div styleName="detail-main">
                <div styleName="player">
                  <Player contentInfo={contentInfo}/>
                </div>
                <div styleName="info">
                  <div styleName="title">{contentInfo.title}</div>
                  <div styleName="description">{contentInfo.description}</div>
                  <div styleName="footer">
                    <div styleName="top">{contentInfo.footerTop}</div>
                    <div styleName="bottom">{contentInfo.footerBottom}</div>
                  </div>
                </div>
              </div>
            );
        }
    }

    render() {
        return (
          <div>
            {this.getDetailPage()}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        contentInfo: state.detailReducer.contentInfo
    };
}

DetailMain.displayName = 'DetailMain';
DetailMain.propTypes = {};
DetailMain.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(DetailMain, styles, {allowMultiple: true}));
