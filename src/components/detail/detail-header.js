import React, {Component} from 'react';
import {connect} from 'react-redux';
import cssmodules from 'react-css-modules';
import styles from './detail.cssmodule.scss';
import * as detailActions from '../../actions/detail-actions';
import apis from '../../static/constants';

export class DetailHeader extends Component {

    componentWillMount() {
        this.props.dispatch(detailActions.getHeaderData(apis.getHeaderData));
    }

    getHeader(title, data) {
        if (data) {
            return (
              <div>
                <div styleName="header-title">
                  <div styleName="title">{title}</div>
                </div>
                <div styleName="header-buttons">
                  {data.map(item =>
                    <div key={item.name} styleName="header-btn">
                      <img styleName="icon" src={item.image}/>
                      <div styleName="title">{item.name}</div>
                    </div>
                        )}
                </div>
              </div>
            );
        }
    }

    render() {
        return (
          <div styleName="detail-header">
            {this.getHeader(this.props.title, this.props.buttonData)}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: state.detailReducer.headerData.title,
        buttonData: state.detailReducer.headerData.buttons
    };
}

DetailHeader.displayName = 'DetailHeadersPage';
DetailHeader.propTypes = {};
DetailHeader.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(DetailHeader, styles, {allowMultiple: true}));
