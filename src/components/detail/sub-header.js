import React, {Component} from 'react';
import {connect} from 'react-redux';
import cssmodules from 'react-css-modules';
import styles from './detail.cssmodule.scss';
import * as detailActions from '../../actions/detail-actions';
import apis from '../../static/constants';

export class SubHeader extends Component {
    constructor() {
        super();
        this.state = {
            activeTab: 0
        };
    }

    componentWillMount() {
        this.props.dispatch(detailActions.getSubHeaderData(apis.getSubHeaderData));
    }

    openSub(index) {
        this.setState({activeTab: index});
    }

    getButtons(data) {
        if (data) {
            return data.map((item, index) =>
              <div
                key={index}
                styleName={this.state.activeTab === index ? 'sub-header-item active' : 'sub-header-item'}
                onClick={this.openSub.bind(this, index)}>{item.name}
              </div>
            );
        }
    }

    render() {

        return (
          <div styleName="detail-sub-header">
            {this.getButtons(this.props.subHeaderData)}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        subHeaderData: state.detailReducer.subHeaderData
    };
}

SubHeader.displayName = 'SubHeadersPage';
SubHeader.propTypes = {};
SubHeader.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(SubHeader, styles, {allowMultiple: true}));
