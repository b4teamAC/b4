import React from 'react';
import cssmodules from 'react-css-modules';
import styles from './slide.cssmodule.scss';
import {Link} from 'react-router';

class Slide extends React.Component {
    render() {
        const {title, imageHref, secondaryTitle, theme, summary, ...other} = this.props;
        return (
          <article styleName="slide">
            <Link to={`/detail:${title}`}>
              <div styleName="imageContainer">
                <img styleName="" src={imageHref}/>
                <img
                  src="http://static.c4assets.com/homepagev1/1.0.202/svg/channel4.svg" styleName="logo"
                  alt="Channel logo"/>
              </div>

              <div styleName="summary">
                {summary}
              </div>

              <header styleName="header red">
                <h2>{title}</h2>
                <h3>{secondaryTitle}</h3>
              </header>
            </Link>
          </article>
        );
    }
}

Slide.displayName = 'Slider';
Slide.propTypes = {};
Slide.defaultProps = {};

export default cssmodules(Slide, styles, {allowMultiple: true});
