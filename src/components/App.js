import React from 'react';
import Slider from './slider/Slider';
import './app.scss';
import Menu from './menu/menu';

class AppComponent extends React.Component {

    render() {
        return (
            <div className="root">
                <Menu />
                <Slider data="sliderData" isTopSlider="true"/>
            </div>
        );
    }
}

AppComponent.defaultProps = {};

export default AppComponent;
