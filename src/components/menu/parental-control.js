import React, {Component} from 'react';
import cssmodules from 'react-css-modules';
import {connect} from 'react-redux';
import * as menuActions from '../../actions/menu-actions';
import styles from './menu.cssmodule.scss';
import apis from '../../static/constants';

class ParentalControl extends Component {
    constructor() {
        super();
        this.state = {
            parentalControl: false
        };
    }

    componentWillMount() {
        this.props.dispatch(menuActions.getRightNavigation(apis.getRightNavigation));
    }

    changeParentalControl() {
        this.setState({
            parentalControl: !this.state.parentalControl
        });
    }

    getParentalControl(data) {
        if (data) {
            return data.map(item =>
              <div
                styleName="list-item parental-control" key={item.itemTitle}
                onClick={this.changeParentalControl.bind(this)}>
                <img
                  styleName="icon-logo" src={this.state.parentalControl ? item.lockedUrl : item.unlockedUrl}
                  alt="icon-logo"/>
                <div styleName="title">
                  {item.itemTitle} {this.state.parentalControl ? 'On' : 'Off'}
                </div>
              </div>
            );
        }
    }

    render() {
        return (
          <div>
            {this.getParentalControl(this.props.parentalControl)}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        parentalControl: state.menuReducer.rightItems.parental
    };
}

ParentalControl.displayName = 'navigationMenu';
ParentalControl.propTypes = {};
ParentalControl.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(ParentalControl, styles, {allowMultiple: true}));

