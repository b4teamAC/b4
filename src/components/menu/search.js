import React, {Component} from 'react';
import cssmodules from 'react-css-modules';
import styles from './menu.cssmodule.scss';
import {connect} from 'react-redux';
import * as menuActions from '../../actions/menu-actions';
import apis from '../../static/constants';

class Search extends Component {

    componentWillMount() {
        this.props.dispatch(menuActions.getRightNavigation(apis.getRightNavigation));
    }

    getSearch(data) {
        if (data) {
            return data.map(item =>
              <div key={item.itemTitle} styleName="list-item search">
                <img styleName="search-logo" src={item.logoUrl} alt="search-logo"/>
                <div styleName="title">{item.itemTitle}</div>
              </div>
            );
        }
    }

    render() {
        return (
          <div>
            {this.getSearch(this.props.search)}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.menuReducer.rightItems.search
    };
}

Search.displayName = 'navigationMenu';
Search.propTypes = {};
Search.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(Search, styles, {allowMultiple: true}));
