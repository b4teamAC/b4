import React, {Component} from 'react';
import * as menuActions from '../../actions/menu-actions';
import {connect} from 'react-redux';
import cssmodules from 'react-css-modules';
import styles from './menu.cssmodule.scss';
import {Link} from 'react-router';
import apis from '../../static/constants';

export class ListItems extends Component {

    componentWillMount() {
        this.props.dispatch(menuActions.getLeftNavigation(apis.getLeftNavigation));
    }

    getLogo(data) {
        if (data) {
            return data.map(image =>
              <div key={image.url} styleName="list-item logo main">
                <img src={image.url} alt="main-logo"/>
              </div>
            );
        }
    }

    getList(data) {
        if (data) {
            return data.map(item =>
              <Link key={item.name} to="/">
                <div styleName={item.main ? 'list-item main' : 'list-item'}>
                  <span>{item.name}</span>
                </div>
              </Link>
            );
        }
    }

    render() {
        return (
          <div>
            {this.getLogo(this.props.image)}
            {this.getList(this.props.listItem)}
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        listItem: state.menuReducer.items.items,
        image: state.menuReducer.items.image
    };
}

ListItems.displayName = 'navigationMenu';
ListItems.propTypes = {};
ListItems.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(ListItems, styles, {allowMultiple: true}));

