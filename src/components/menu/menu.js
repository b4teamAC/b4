import React, {Component} from 'react';
import cssmodules from 'react-css-modules';
import styles from './menu.cssmodule.scss';
import ListItems from './list-items';
import ParentalControl from './parental-control';
import Search from './search';

export class Menu extends Component {

    render() {
        return (
          <div styleName="main-menu">
            <div styleName="left-part">
              <ListItems />
            </div>
            <div styleName="right-part">
              <ParentalControl />
              <Search />
            </div>
          </div>
        );
    }
}

Menu.displayName = 'navigationMenu';
Menu.propTypes = {};
Menu.defaultProps = {};

export default cssmodules(Menu, styles);
