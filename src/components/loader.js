import React from 'react';

const Loader = function (props) {
    return (
      <div className="loader">
        <img
          src="http://orig03.deviantart.net/2fab/f/2014/017/2/2/work_in_progress_by_exohazard-d72iavi.gif"
          alt="loader"/>
      </div>
    );
};

export default Loader;
