import React from 'react';
import cssmodules from 'react-css-modules';
import styles from './slider.cssmodule.scss';
import {connect} from 'react-redux';
import Slide from '../slide/Slide';
import Loader from '../../components/loader';
import * as sliderActions from '../../actions/slider/Slider';

class Slider extends React.Component {
    componentWillMount() {
        this.props.dispatch(sliderActions.getSliderData());
    }

    render() {
        const {slideList, isTopSlider, sliceName, isLoading, ...other} = this.props;

        let slide = [];
        if (slideList) {
            slide = slideList.map(slide =>
                <Slide
                    key={slide.title}
                    isTopSlider={isTopSlider}
                    title={slide.title}
                    secondaryTitle={slide.secondaryTitle}
                    theme={slide.theme}
                    summary={slide.summary}
                    imageHref={slide.image.href}
                />
            );
        }

        return (
            <div styleName="container">
                {isLoading ? (<Loader />) : ''}
                <header styleName="header">
                    <h2>{sliceName}</h2>
                </header>

                <div styleName="slider">{slide}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        sliceName: state.sliderReducer.slice.title,
        slideList: state.sliderReducer.slice.sliceItems,
        isLoading: state.sliderReducer.isLoading
    };
}

Slider.displayName = 'SliderSlider';
Slider.propTypes = {};
Slider.defaultProps = {};

export default connect(mapStateToProps)(cssmodules(Slider, styles));
