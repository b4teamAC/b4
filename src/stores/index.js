import {applyMiddleware, createStore} from 'redux';
import reducers from '../reducers';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

function reduxStore(initialState) {
    const middleware = applyMiddleware(promise(), thunk);

    const store = createStore(reducers, middleware, initialState,
        window.devToolsExtension && window.devToolsExtension());

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            // We need to require for hot reloading to work properly.
            const nextReducer = require('../reducers');  // eslint-disable-line global-require

            store.replaceReducer(nextReducer);
        });
    }

    return store;
}

export default reduxStore;

