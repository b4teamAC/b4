const defaultMenuState = {
    items: [],
    rightItems: []
};

export default function reducer(state = defaultMenuState, action) {
    switch (action.type) {
    case 'GET_MENU_ITEMS': {
        return {
            ...state,
            items: action.payload
        };
    }
    case 'GET_RIGHT_ITEMS': {
        return {
            ...state,
            rightItems: action.payload
        };
    }
    }
    return state;
}

module.exports = reducer;
