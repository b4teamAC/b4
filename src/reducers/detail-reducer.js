const defaultDetailState = {
    headerData: [],
    subHeaderData: [],
    contentInfo: null
};

export default function reducer(state = defaultDetailState, action) {
    switch (action.type) {
    case 'GET_HEADER': {
        return {
            ...state,
            headerData: action.payload
        };
    }
    case 'GET_SUB_HEADER': {
        return {
            ...state,
            subHeaderData: action.payload
        };
    }
    case 'GET_MAIN_DETAIL': {
        return {
            ...state,
            contentInfo: action.payload
        };
    }
    }
    return state;
}

module.exports = reducer;
