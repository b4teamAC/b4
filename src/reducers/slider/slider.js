const defaultState = {
    slice: [],
    isLoading: true
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
    case 'GET_SLIDER_ITEMS': {
        return {
            ...state,
            slice: action.payload.slices[0],
            isLoading: false
        };
    }
    }
    return state;
}

module.exports = reducer;
