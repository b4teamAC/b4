import axios from 'axios';

export function getLeftNavigation(path) {
    return function (dispatch) {
        axios.get(path).then((response) => {
            dispatch({type: 'GET_MENU_ITEMS', payload: response.data});
        }).catch((err) => {
            dispatch({type: 'GET_MENU_ITEMS_REJECTED', payload: err.response.data});
        });
    };
}

export function getRightNavigation(path) {
    return function (dispatch) {
        axios.get(path).then((response) => {
            dispatch({type: 'GET_RIGHT_ITEMS', payload: response.data});
        }).catch((err) => {
            dispatch({type: 'GET_RIGHT_ITEMS_REJECTED', payload: err.response.data});
        });
    };
}
