import axios from 'axios';

export function getSliderData() {
    return function (dispatch) {
        axios.get('http://www.channel4.com/api/homepage?').then((response) => {
            dispatch({type: 'GET_SLIDER_ITEMS', payload: response.data});
        }).catch((err) => {
            dispatch({type: 'GET_SLIDER_ITEMS_REJECTED', payload: err.data});
        });
    };
}
