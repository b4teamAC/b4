import axios from "axios";

export function getHeaderData(path) {
    return function (dispatch) {
        axios.get(path).then((response) => {
            dispatch({type: 'GET_HEADER', payload: response.data.response});
        }).catch((err) => {
            dispatch({type: 'GET_HEADER_REJECTED', payload: err.response.data});
        });
    };
}
export function getSubHeaderData(path) {
    return function (dispatch) {
        axios.get(path).then((response) => {
            dispatch({type: 'GET_SUB_HEADER', payload: response.data.response});
        }).catch((err) => {
            dispatch({type: 'GET_SUB_HEADER_REJECTED', payload: err.response.data});
        });
    };
}

export function getDetailMainData(path) {
    return function (dispatch) {
        axios.get(path).then((response) => {
            dispatch({type: 'GET_MAIN_DETAIL', payload: response.data.response});
        }).catch((err) => {
            dispatch({type: 'GET_MAIN_DETAIL_REJECTED', payload: err.response.data});
        });
    };
}

